package cn.stronglink.esm27.algorithm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Esm27AlgorithmServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(Esm27AlgorithmServiceApplication.class, args);
	}
}
