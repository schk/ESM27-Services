package cn.stronglink.esm27.algorithm.core.message;
/**
 * 消息发送器
 * @author yuzhantao
 *
 */
public interface IMessageSender<T> {
	void sendMessage(T datas);
}
