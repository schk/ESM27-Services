package cn.stronglink.esm27.algorithm.core.message;

import java.util.ArrayList;
import java.util.List;

import io.netty.channel.ChannelHandlerContext;

public class MessageHandleContext<T,V> implements IMessageHandle<T,V> {
	private List<IMessageHandle<T,V>> handleList = new ArrayList<>();
	
	@Override
	public boolean isHandle(T t) {
		return true;
	}
	
	@Override
	public V handle(ChannelHandlerContext ctx, T t) {
		for(IMessageHandle<T,V> msg:handleList){
			if(msg.isHandle(t)){
				return msg.handle(ctx,t);
			}
		}
		return null;
	}
	
	/**
	 * 添加处理类
	 * @param handle
	 */
	public void addHandleClass(IMessageHandle<T,V> handle){
		handleList.add(handle);
	}
	
	/**
	 * 清除所有处理累
	 */
	public void clearHandleClass() {
		handleList.clear();
	}
}
