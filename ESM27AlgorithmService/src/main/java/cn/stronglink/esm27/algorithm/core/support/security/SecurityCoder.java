/**
 * 2011-01-11
 */
package cn.stronglink.esm27.algorithm.core.support.security;

import java.security.Security;

/**
 * 加密基类
 * 
 * @author lei_w
 * @version 1.0
 * @since 1.0
 */
public abstract class SecurityCoder {
    private static Byte ADDFLAG = 0;
    static {
        if (ADDFLAG == 0) {
            // 加入BouncyCastleProvider支持
            Security.addProvider(new BouncyCastleProvider());
            ADDFLAG = 1;
        }
    }
}
