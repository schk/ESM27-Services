package cn.stronglink.esm27.algorithm.iot;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;


public class PathUtil {
	
	public static String getClassPath() {

		String xmlpath = "";
		try {
			xmlpath = URLDecoder.decode(PathUtil.class.getClassLoader().getResource("").getPath(),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			try {
				throw new Exception("数据编码错误");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		xmlpath = xmlpath.replaceFirst("/", "").replace("/", "\\");
		return xmlpath;
	}
}
