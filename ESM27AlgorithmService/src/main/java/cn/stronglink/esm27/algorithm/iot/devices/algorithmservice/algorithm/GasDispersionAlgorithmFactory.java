package cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.algorithm;

public class GasDispersionAlgorithmFactory implements IAlgorithmFactory {

	@Override
	public IAlgorithm createAlgorithm() {
		return new GasDispersionAlgorithm();
	}

}
