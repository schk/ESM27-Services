package cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.algorithm;

/**
 * 算法接口
 * @author yuzhantao
 *
 */
public interface IAlgorithm {
	/**
	 * 更新算法的参数
	 * @param params
	 */
	void updateParams(Object[] params);
	/**
	 * 计算结果
	 * @return
	 */
	Object calc();
}
