package cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.algorithm;

/**
 * 算法工厂
 * @author yuzhantao
 *
 */
public interface IAlgorithmFactory {
	/**
	 * 创建算法
	 * @return
	 */
	IAlgorithm createAlgorithm();
}
