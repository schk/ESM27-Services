package cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.algorithm;

import com.sun.jna.win32.StdCallLibrary;

interface MathModelAPI extends StdCallLibrary {
	/**
	 * 鍒濆鍖栬绠楁皵浣揂PI
	 * 
	 * @return
	 */
	public int Init_API();

	/*****************************************
	 * 娉勬紡婧愬弽绠�
	 ***********************************************/

	/**
	 * 璁＄畻娉勬紡婧愮殑浣嶇疆鍜屾硠婕忕巼 ws: 椋庨�燂紝鍗曚綅m/s wd: 椋庡悜锛屽崟浣嶅害 as: 澶ф皵绋冲畾搴︼紝鍙栧�� 1,2,3,4,5,6 涓庡ぇ姘旂ǔ瀹氬害鍙栧�艰〃鐨勫搴斿叧绯�
	 * 1=A, 2=B, 3=C, 4=D, 5=E, 6=F data[]:
	 * 鍥涚粍浼犳劅鍣ㄦ暟鎹紝浼犲��(q1,x1,y1,h1,......q4,x4,y4,h4) q娴撳害锛寈y浼犳劅鍣ㄥ潗鏍囷紝h浼犳劅鍣ㄨ窛绂诲湴闈㈤珮搴� 杩斿洖鍊�:
	 * true璁＄畻鎴愬姛锛宖alse澶辫触
	 */
	public boolean LeakSource_API(double ws, double wd, int as, final String data);

	/**
	 * 鑾峰彇娉勬紡鐜� 杩斿洖鍊�: 娉勬紡鐜囷紝鍗曚綅鍏枻姣忕(Kg/m)
	 */
	public double GetLsQ_API();

	/**
	 * 鑾峰彇娉勬紡婧愬潗鏍� x 杩斿洖鍊�: x 鍧愭爣锛屽崟浣嶇背(m)
	 */
	public double GetLsX_API();

	/**
	 * 鑾峰彇娉勬紡婧愬潗鏍� y 杩斿洖鍊�: y 鍧愭爣锛屽崟浣嶇背(m)
	 */
	public double GetLsY_API();

	/**
	 * 鑾峰彇娉勬紡婧愰珮搴� h 杩斿洖鍊�: 楂樺害锛屽崟浣嶇背(m)
	 */
	public double GetLsH_API();

	/*****************************************
	 * 姘斾綋鎵╂暎楂樻柉妯″瀷
	 ***********************************************/

	/**
	 * 璁剧疆姘斾綋鎵╂暎浼犲弬鏁�
	 * 
	 * @param as 澶ф皵绋冲畾搴︼紝鏁村瀷锛屽彇鍊� 1,2,3,4,5,6 涓庡ぇ姘旂ǔ瀹氬害鍙栧�艰〃鐨勫搴斿叧绯� 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
	 * @param ws 鐜椋庨�燂紝涓�鑸彇鍦伴潰 10m 楂樺鐨勫钩鍧囬閫燂紝鍗曚綅m/s
	 * @param wd 椋庡悜锛屽崟浣嶅害
	 * @param gd 姹℃煋姘斾綋鍦ㄥ父娓╀笅鐨勬祿搴︼紝鍗曚綅 kg/m3
	 * @param h  娉勬紡鐐圭鍦伴潰鐨勯珮搴�
	 * @param dq 娉勬紡婧愬己搴︼紝kg/s
	 * @return true鎴愬姛锛宖alse澶辫触
	 */
	public boolean SetGaussionModelParameters_API(int as, double ws, double wd, double gd, double h, double dq);

	/**
	 * 璁剧疆绉垎鏃堕棿闂撮殧锛屼篃鍙互涓嶈缃鍙傛暟锛岀▼搴忓唴榛樿鏄� 1 绉掞紝闂撮殧瓒婄煭璁＄畻娆℃暟瓒婂鑰楁椂瓒婇暱锛岄棿闅旇秺闀胯绠楁鏁拌秺灏戣�楁椂瓒婂皯銆� dt: 绉垎鏃堕棿闂撮殧锛屽崟浣嶇s
	 * 杩斿洖鍊�: true鎴愬姛锛宖alse澶辫触
	 */
	public boolean SetDeltaT_API(double dt);

	/**
	 * 寮�濮嬫ā鎷燂紝璁＄畻 fCurrentTime 鏃堕棿鐐圭殑鑼冨洿杞粨绾� fCurrentTime: 鎵╂暎鏃堕棿 杩斿洖鍊� true 璁＄畻鎴愬姛锛宖alse 璁＄畻澶辫触
	 */
	public boolean Simulate_API(double fCurrentTime);

	/**
	 * 璁剧疆娴撳害闃堝�硷紝璁＄畻姝ゆ祿搴﹁寖鍥磋疆寤撶嚎锛屽崟浣峩g/m3 t: 绛夊�肩嚎娴撳害
	 */
	public void SetThreshold_API(double t);

	/**
	 * 鑾峰彇杞粨绾块《鐐瑰潗鏍囨暟缁勭殑闀垮害
	 */
	public int GetVerticesLength_API();

	/**
	 * 鑾峰彇杞粨绾块《鐐瑰潗鏍囨暟缁刐x1,y1,z1,x2,y2,z2,......]
	 */
	public void GetVertices_API(double va[]);

	/*****************************************
	 * 棰楃矑鐗╂矇闄嶆ā鍨�
	 ***********************************************/
	/**
	 * 浼犲弬鏁� as: 澶ф皵绋冲畾搴︼紝鏁村瀷锛屽彇鍊� 1,2,3,4,5,6 涓庡ぇ姘旂ǔ瀹氬害鍙栧�艰〃鐨勫搴斿叧绯� 1=A, 2=B, 3=C, 4=D, 5=E, 6=F
	 * ws: 鐜椋庨�燂紝涓�鑸彇鍦伴潰 10m 楂樺鐨勫钩鍧囬閫燂紝鍗曚綅m/s wd: 椋庡悜锛屽崟浣嶅害 h: 鐑熼浘娉勬紡鐐圭鍦伴潰鐨勯珮搴� dq: 棰楃矑鐗╂簮寮哄害锛宬g/s
	 * 杩斿洖鍊�: true鎴愬姛锛宖alse澶辫触
	 */
	public boolean SetPsParameters_API(int as, double ws, double wd, double gd, double h, double dq);

	/**
	 * 璁剧疆绉垎鏃堕棿闂撮殧锛屼篃鍙互涓嶈缃鍙傛暟锛岀▼搴忓唴榛樿鏄� 1 绉掞紝闂撮殧瓒婄煭璁＄畻娆℃暟瓒婂鑰楁椂瓒婇暱锛岄棿闅旇秺闀胯绠楁鏁拌秺灏戣�楁椂瓒婂皯銆� dt: 绉垎鏃堕棿闂撮殧锛屽崟浣嶇s
	 * 杩斿洖鍊�: true鎴愬姛锛宖alse澶辫触
	 */
	public boolean SetPsDeltaT_API(double dt);

	/**
	 * 寮�濮嬫ā鎷燂紝璁＄畻 fCurrentTime 鏃堕棿鐐圭殑鑼冨洿杞粨绾� fCurrentTime: 鎵╂暎鏃堕棿 杩斿洖鍊� true 璁＄畻鎴愬姛锛宖alse 璁＄畻澶辫触
	 */
	public boolean SimulatePs_API(double fCurrentTime);

	/**
	 * 璁剧疆娌夐檷璐ㄩ噺闃堝�硷紝璁＄畻姝ゆ矇闄嶈川閲忚寖鍥磋疆寤撶嚎锛屽崟浣峩g/m3 t: 绛夊�肩嚎
	 */
	public void SetPsThreshold_API(double t);

	/**
	 * 鑾峰彇杞粨绾块《鐐瑰潗鏍囨暟缁勭殑闀垮害
	 */
	public int GetPsVerticesLength_API();

	/**
	 * 鑾峰彇杞粨绾块《鐐瑰潗鏍囨暟缁刐x1,y1,x2,y2,......]
	 */
	public void GetPsVertices_API(double va[]);

	/***************************************
	 * 鐖嗙偢妯″瀷
	 *************************************************/
	/**
	 * 璁＄畻鐖嗙偢浼ゅ type: 鐖嗙偢绫诲瀷(1-3鏄墿鐞嗙垎鐐�, 4鏄寲瀛︾垎鐐�), 1 鍘嬬缉姘斾綋瀹瑰櫒鐖嗙偢, 2 楂樺帇娑蹭綋瀹瑰櫒鐖嗙偢, 3 杩囩儹娑蹭綋瀹瑰櫒鐖嗙偢, 4
	 * 钂告皵浜戠垎鐐� idx: 鐖嗙偢鐗╃储寮� 1
	 * 鍘嬬缉姘斾綋瀹瑰櫒鐖嗙偢:绌烘皵(1),姘�(2),姘�(3),姘�(4),鐢茬兎(5),涔欑兎(6),涔欑儻(7),涓欑兎(8),涓�姘у寲纰�(9),浜屾哀鍖栫⒊(10),涓�姘у寲姘�(11),浜屾哀鍖栨爱(12),姘ㄦ皵(13),姘皵(14),杩囩儹钂告皵(15),骞查ケ鍜岃捀姘�(16),姘㈡隘閰�(17)
	 * 2 楂樺帇娑蹭綋瀹瑰櫒鐖嗙偢:鏃� 3 杩囩儹娑蹭綋瀹瑰櫒鐖嗙偢:楂樻俯楗卞拰姘�(1) 4
	 * 钂告皵浜戠垎鐐�:姘㈡皵(1),姘ㄦ皵(2),鑻�(3),涓�姘у寲纰�(4),纭寲姘�1(鐢熸垚SO2)(5),纭寲姘�2(鐢熸垚SO3)(6),鐢茬兎(7),涔欑兎(8),涔欑儻(9),涔欑倲(10),涓欑兎(11),涓欑儻(12),姝ｄ竵鐑�(13),寮備竵鐑�(14),涓佺儻(15)
	 * pressure: 鐖嗙偢鐗╃殑鍘嬪姏, 鍗曚綅鍏嗗笗(Mpa) volume: 鐖嗙偢鐗╀綋绉�, 鍗曚綅绔嬫柟绫�(m鲁) r: 璺濈鐖嗙偢鐐圭殑璺濈 杩斿洖鍊�:
	 * 浼ゅ鍊�(鏌ヨ〃鑾峰緱浼ゅ鎻忚堪)
	 */
	public double CalcExplosionHurt_API(int type, int idx, double pressure, double volume, double r);

	/**
	 * 璁＄畻鐖嗙偢浼ゅ鍗婂緞 type: 鐖嗙偢绫诲瀷(1-3鏄墿鐞嗙垎鐐�, 4鏄寲瀛︾垎鐐�), 1 鍘嬬缉姘斾綋瀹瑰櫒鐖嗙偢, 2 楂樺帇娑蹭綋瀹瑰櫒鐖嗙偢, 3 杩囩儹娑蹭綋瀹瑰櫒鐖嗙偢, 4
	 * 钂告皵浜戠垎鐐� idx: 鐖嗙偢鐗╃储寮� 1
	 * 鍘嬬缉姘斾綋瀹瑰櫒鐖嗙偢:绌烘皵(1),姘�(2),姘�(3),姘�(4),鐢茬兎(5),涔欑兎(6),涔欑儻(7),涓欑兎(8),涓�姘у寲纰�(9),浜屾哀鍖栫⒊(10),涓�姘у寲姘�(11),浜屾哀鍖栨爱(12),姘ㄦ皵(13),姘皵(14),杩囩儹钂告皵(15),骞查ケ鍜岃捀姘�(16),姘㈡隘閰�(17)
	 * 2 楂樺帇娑蹭綋瀹瑰櫒鐖嗙偢:鏃� 3 杩囩儹娑蹭綋瀹瑰櫒鐖嗙偢:楂樻俯楗卞拰姘�(1) 4
	 * 钂告皵浜戠垎鐐�:姘㈡皵(1),姘ㄦ皵(2),鑻�(3),涓�姘у寲纰�(4),纭寲姘�1(鐢熸垚SO2)(5),纭寲姘�2(鐢熸垚SO3)(6),鐢茬兎(7),涔欑兎(8),涔欑儻(9),涔欑倲(10),涓欑兎(11),涓欑儻(12),姝ｄ竵鐑�(13),寮備竵鐑�(14),涓佺儻(15)
	 * pressure: 鐖嗙偢鐗╃殑鍘嬪姏, 鍗曚綅鍏嗗笗(Mpa) volume: 鐖嗙偢鐗╀綋绉�, 鍗曚綅绔嬫柟绫�(m鲁) hurt: 浼ゅ鍊� 杩斿洖鍊�:
	 * 鑳藉閫犳垚姝や激瀹冲�肩殑鍗婂緞鑼冨洿
	 */
	public double CalcExplosionHurtR_API(int type, int idx, double pressure, double volume, double hurt);

	/*****************************************
	 * 鐑緪灏勬ā鍨�
	 ***********************************************/
	/**
	 * 浼犲弬鏁帮紙璁＄畻鐑緪灏勪激瀹筹級锛屽悗 4 涓弬鏁版煡鏁版嵁搴� S: 鐏伨闈㈢Н锛屽崟浣嶅钩鏂圭背(m2) T0: 鐜娓╁害锛屽崟浣嶅紑姘忓害(K) Hc:
	 * 鐗╄川鐨勭噧鐑х儹锛屽崟浣嶇劍鑰虫瘡鍏枻(J/Kg) Hv: 鐗╄川鐨勮捀鍙戠儹锛屽崟浣嶇劍鑰虫瘡鍏枻(J/Kg) cp:
	 * 鐗╄川鐨勫畾鍘嬫瘮鐑锛屽崟浣嶇劍鑰�(姣忓叕鏂ゆ瘡鍗庢皬搴�)(J/(Kg*K)) Tb: 鐗╄川鐨勬哺鐐癸紝鍗曚綅寮�姘忓害(K) 杩斿洖鍊�: true鎴愬姛锛宖alse澶辫触
	 */
	public boolean SetHeatHurtParameters_API(double S, double T0, double Hc, double Hv, double cp, double Tb);

	/**
	 * 璁＄畻鐑緪灏勪激瀹� length: 璺濈鐫�鐏腑蹇冪偣璺濈锛屽崟浣嶇背(m) time: 鏆撮湶鍦ㄧ儹杈愬皠鑼冨洿鍐呯殑鏃堕棿锛屽崟浣嶇(m) 杩斿洖鍊�:
	 * 鐩爣鍦扮偣鎺ユ敹鍒扮殑鐑�氶噺瀵嗗害锛屽崟浣嶅崈鐡︽瘡骞虫柟绫筹紙kW/m2锛�
	 */
	public double CalcHeatHurt_API(double length, double time);

	/**
	 * 鑾峰彇姝讳骸姒傜巼 杩斿洖鍊�: 姝讳骸姒傜巼锛屾鐜囧崟浣嶄负5鏃跺搴斾激浜＄櫨鍒嗘暟50%
	 */
	public double GetHeatHurtPr1_API();

	/**
	 * 鑾峰彇浜岀骇鐑т激姒傜巼 杩斿洖鍊�: 浜岀骇鐑т激姒傜巼锛屾鐜囧崟浣嶄负5鏃跺搴斾激浜＄櫨鍒嗘暟50%
	 */
	public double GetHeatHurtPr2_API();

	/**
	 * 鑾峰彇涓�绾х儳浼ゆ鐜� 杩斿洖鍊�: 涓�绾х儳浼ゆ鐜囷紝姒傜巼鍗曚綅涓�5鏃跺搴斾激浜＄櫨鍒嗘暟50%
	 */
	public double GetHeatHurtPr3_API();

	/**
	 * 璁＄畻鐑緪灏勪激瀹冲崐寰� time: 鏆撮湶鍦ㄧ儹杈愬皠鑼冨洿鍐呯殑鏃堕棿
	 */
	public void CalcHeatHurtR_API(double time);

	/**
	 * 鑾峰彇姝讳骸鍗婂緞锛堟鐜囧崟浣嶅ぇ浜庣瓑浜�5锛� 杩斿洖鍊�: 姝讳骸鍗婂緞锛屽崟浣嶇背(m)
	 */
	public double GetHeatHurtR1_API();

	/**
	 * 鑾峰彇浜岀骇鐑т激鍗婂緞锛堟鐜囧崟浣嶅ぇ浜庣瓑浜�5锛� 杩斿洖鍊�: 浜岀骇鐑т激鍗婂緞锛屽崟浣嶇背(m)
	 */
	public double GetHeatHurtR2_API();

	/**
	 * 鑾峰彇涓�绾х儳浼ゅ崐寰勶紙姒傜巼鍗曚綅澶т簬绛変簬5锛� 杩斿洖鍊�: 涓�绾х儳浼ゅ崐寰勶紝鍗曚綅绫�(m)
	 */
	public double GetHeatHurtR3_API();

	/*****************************************
	 * 娑蹭綋娉勬紡妯″瀷
	 ***********************************************/
	/**
	 * 璁＄畻娑蹭綋娉勬紡鐜� rou: 娑蹭綋瀵嗗害锛坘g/m3锛� s: 娉勯湶瀛旈潰绉紙m2锛� p: 瀹瑰櫒鍐呭帇寮猴紙Pa锛� h:
	 * h娑插帇楂樺害锛堟恫浣撴渶楂樼偣涓庢硠婕忕偣涔嬮棿鐨勯珮搴﹀樊锛夛紙m锛� 杩斿洖鍊�: 娉勬紡閫熺巼锛坘g/s锛�
	 */
	public double CalcLiquidLeak_API(double rou, double s, double p, double h);

	/**
	 * 璁＄畻娑蹭綋娉勬紡锛堢灛鏃舵硠闇诧級鍚庡舰鎴愮殑娑叉睜鍗婂緞 m: 娉勬紡鎬婚噺锛坘g锛� t: 娉勬紡鏃堕棿锛坰锛� rou: 娑蹭綋瀵嗗害锛坘g/m3锛� 杩斿洖鍊�: 娑叉睜鍗婂緞锛坢锛�
	 */
	public double CalcLiquidLeakArea1_API(double m, double t, double rou);

	/**
	 * 璁＄畻娑蹭綋娉勬紡锛堟寔缁硠闇诧級鍚庡舰鎴愮殑娑叉睜鍗婂緞 m: 娉勬紡鎬婚噺锛坘g锛� t: 娉勬紡鏃堕棿锛坰锛� rou: 娑蹭綋瀵嗗害锛坘g/m3锛� 杩斿洖鍊�: 娑叉睜鍗婂緞锛坢锛�
	 */
	public double CalcLiquidLeakArea2_API(double m, double t, double rou);

	/*****************************************
	 * 钂稿彂妯″瀷
	 ***********************************************/
	/**
	 * 璁＄畻闂捀锛岃捀鍙戦�熺巼 Cp:瀹氬帇姣旂儹锛圝鈥g-1鈥-1锛� Hv: 钂稿彂鐑紙J/kg锛� T0: 娓╁害锛堝紑姘忓害K锛� Tb: 娌哥偣锛堝紑姘忓害K锛� m:
	 * 娉勬紡璐ㄩ噺锛坘g锛� t: 闂捀鏃堕棿锛坰锛� 杩斿洖鍊�: 闂捀閫熺巼锛坘g/s锛�
	 */
	public double CalcEvaporate1_API(double Cp, double Hv, double T0, double Tb, double m, double t);

	/**
	 * 璁＄畻鐑噺钂稿彂锛岃捀鍙戦�熺巼 A1: 娑叉睜闈㈢Н锛坢2锛� T0: 鐜娓╁害锛堝紑姘忓害K锛� Tb: 娑蹭綋娌哥偣锛堝紑姘忓害K锛� Hv: 钂稿彂鐑紙J/kg锛� L:
	 * 娑叉睜闀垮害锛坢锛� K: 鍦伴潰瀵肩儹绯绘暟锛圝鈥-1鈥-1锛� a: 鐑墿鏁ｇ郴鏁帮紙m2/s锛� t: 钂稿彂鏃堕棿锛坰锛� Nu: 鍔灏旀暟 杩斿洖鍊�:
	 * 钂稿彂閫熺巼锛坘g/s锛�
	 */
	public double CalcEvaporate2_API(double A1, double T0, double Tb, double Hv, double L, double K, double a, double t,
			double Nu);

	/**
	 * 璁＄畻璐ㄩ噺钂稿彂锛岃捀鍙戦�熺巼 alpha: 鍒嗗瓙鎵╂暎绯绘暟锛坢2/s锛� Sh: 鑸嶄紞寰锋暟 A: 娑叉睜闈㈢Н锛坢2锛� L: 娑叉睜闀垮害锛坢锛� rou:
	 * 娑蹭綋瀵嗗害锛坘g/m3锛� 杩斿洖鍊�: 钂稿彂閫熺巼锛坘g/s锛�
	 */
	public double CalcEvaporate3_API(double alpha, double Sh, double A, double L, double rou);

	/*****************************************
	 * 鐭虫补鐕冪儳鐗╁垎鏋�
	 ***********************************************/
	/**
	 * 璁＄畻鐭虫补鐕冪儳鐗╀腑姹℃煋鐗╃殑浜х敓鐜� speed: 椋庨�� m/s mt: 娌瑰搧鐕冪儳閫熷害 kg/s 鎴� g/s Cc: 鐭虫补涓惈纰崇殑璐ㄩ噺鐧惧垎姣� Cs:
	 * 鐭虫补涓惈纭殑璐ㄩ噺鐧惧垎姣� Cn: 鐭虫补涓惈姘殑璐ㄩ噺鐧惧垎姣�
	 */
	public void CalcEmissionRate_API(double speed, double mt, double Cc, double Cs, double Cn);

	/**
	 * 鑾峰彇 CO 浜х敓鐜� 杩斿洖鍊�: CO 浜х敓鐜�(鍗曚綅涓� CalcEmission_API 涓� mt 鐨勫崟浣嶄竴鑷达紝鍚屼负 kg/s 鎴� g/s)
	 */
	public double GetCO_API();

	/**
	 * 鑾峰彇 CO2 浜х敓鐜� 杩斿洖鍊�: CO2 浜х敓鐜�(鍗曚綅涓� CalcEmission_API 涓� mt 鐨勫崟浣嶄竴鑷达紝鍚屼负 kg/s 鎴� g/s
	 */
	public double GetCO2_API();

	/**
	 * 鑾峰彇 SO2 浜х敓鐜� 杩斿洖鍊�: SO2 浜х敓鐜�(鍗曚綅涓� CalcEmission_API 涓� mt 鐨勫崟浣嶄竴鑷达紝鍚屼负 kg/s 鎴� g/s
	 */
	public double GetSO2_API();

	/**
	 * 鑾峰彇 NO 浜х敓鐜� 杩斿洖鍊�: NO 浜х敓鐜�(鍗曚綅涓� CalcEmission_API 涓� mt 鐨勫崟浣嶄竴鑷达紝鍚屼负 kg/s 鎴� g/s
	 */
	public double GetNO_API();

	/**
	 * 鑾峰彇 NO2 浜х敓鐜� 杩斿洖鍊�: NO2 浜х敓鐜�(鍗曚綅涓� CalcEmission_API 涓� mt 鐨勫崟浣嶄竴鑷达紝鍚屼负 kg/s 鎴� g/s
	 */
	public double GetNO2_API();
}
