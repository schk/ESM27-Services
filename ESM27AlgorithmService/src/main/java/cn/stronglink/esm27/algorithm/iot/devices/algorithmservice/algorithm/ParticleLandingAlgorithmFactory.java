package cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.algorithm;

public class ParticleLandingAlgorithmFactory implements IAlgorithmFactory {

	@Override
	public IAlgorithm createAlgorithm() {
		return new ParticleLandingAlgorithm();
	}

}
