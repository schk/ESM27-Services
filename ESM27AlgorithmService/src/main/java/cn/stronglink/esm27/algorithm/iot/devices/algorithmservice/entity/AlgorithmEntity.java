package cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.entity;

public class AlgorithmEntity {
	/**
	 * 计算结果
	 */
	private double algorithmResult;
	/**
	 * 源数据
	 */
	private Object srcData;
	
	public AlgorithmEntity(Object srcData) {
		this.setSrcData(srcData);
	}
	
	public double getAlgorithmResult() {
		return algorithmResult;
	}
	public void setAlgorithmResult(double algorithmResult) {
		this.algorithmResult = algorithmResult;
	}
	public Object getSrcData() {
		return srcData;
	}
	public void setSrcData(Object srcData) {
		this.srcData = srcData;
	}
	
	
}
