package cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.entity;

/**
 * 设备实体
 * @author yuzhantao
 *
 */
public class DeviceEntity {
	/**
	 * 设备地址
	 */
	private byte deviceAddress;

	public byte getDeviceAddress() {
		return deviceAddress;
	}

	public void setDeviceAddress(byte deviceAddress) {
		this.deviceAddress = deviceAddress;
	}

}
