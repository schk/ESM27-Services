package cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.entity;

public class GasAlgorithmManager {
	private static GasDispersionParamsEntity gasDispersionParamsEntity;
	
	public static void setGasDispersionParams(GasDispersionParamsEntity entity) {
		gasDispersionParamsEntity = entity;
	}
	
	public static GasDispersionParamsEntity getGasDispersionParams() {
		return gasDispersionParamsEntity;
	}
}
