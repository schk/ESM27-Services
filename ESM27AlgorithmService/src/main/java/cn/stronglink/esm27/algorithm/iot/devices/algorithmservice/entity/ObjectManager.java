package cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.entity;

import java.util.HashMap;
import java.util.Map;

public class ObjectManager<T> {
	protected Map<Byte,T> map = new HashMap<>();
	
	@SuppressWarnings("rawtypes")
	private static ObjectManager instance;
	
	@SuppressWarnings("rawtypes")
	public static ObjectManager getInstnce() {
		if(instance==null) {
			instance=new ObjectManager();
		}
		return instance;
	}
	public void put(byte code,T t) {
		this.map.put(code, t);
	}
	
	public T get(byte code) {
		return this.map.get(code);
	}
}
