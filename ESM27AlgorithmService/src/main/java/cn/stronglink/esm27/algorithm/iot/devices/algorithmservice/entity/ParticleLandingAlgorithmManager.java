package cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.entity;

public class ParticleLandingAlgorithmManager {
	private static GasDispersionParamsEntity gasDispersionParamsEntity;
	
	public static void setParticleLandingParams(GasDispersionParamsEntity entity) {
		gasDispersionParamsEntity = entity;
	}
	
	public static GasDispersionParamsEntity getParticleLandingParams() {
		return gasDispersionParamsEntity;
	}
}
