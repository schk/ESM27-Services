package cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.request;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.google.gson.reflect.TypeToken;

import cn.stronglink.esm27.algorithm.core.message.MessageHandleContext;
import cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.message.MQMessageOfESM27;
import cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.request.handle.CloseGasDispersionAlgorithmServiceHandle;
import cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.request.handle.CloseParticleLandingAlgorithmServiceHandle;
import cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.request.handle.StartGasDispersionAlgorithmServiceHandle;
import cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.request.handle.StartParticleLandingAlgorithmServiceHandle;

@Component
public class MQMessageOfEMS27Listener extends MessageListenerAdapter {
	private static Logger logger = LogManager.getLogger(MQMessageOfEMS27Listener.class.getName());
	// 通过策略模式处理不同的信息
	private MessageHandleContext<MQMessageOfESM27, Object> messageHandleContent;

	public MQMessageOfEMS27Listener() {
		super();
		this.messageHandleContent = new MessageHandleContext<>();
//		this.messageHandleContent.addHandleClass(new PullSensorInfoHandle());
//		this.messageHandleContent.addHandleClass(new PullWindInfoHandle());
		this.messageHandleContent.addHandleClass(new StartGasDispersionAlgorithmServiceHandle());
		this.messageHandleContent.addHandleClass(new CloseGasDispersionAlgorithmServiceHandle());
		this.messageHandleContent.addHandleClass(new StartParticleLandingAlgorithmServiceHandle());
		this.messageHandleContent.addHandleClass(new CloseParticleLandingAlgorithmServiceHandle());
	}

	@JmsListener(destination = "ServiceToESM27Algorithm", concurrency = "1")
	public void onMessage(Message message, Session session) throws JMSException {
		if (message instanceof TextMessage) {
			try {
				TextMessage tm = (TextMessage) message;
				String json = tm.getText();
				logger.debug("MQ接收到json数据:" + json);
				MQMessageOfESM27 rm = JSON.parseObject(json, new TypeToken<MQMessageOfESM27>() {
				}.getType());
				this.messageHandleContent.handle(null, rm);
			} catch (Exception e) {
				logger.error(e);
			}
		} else {
			logger.info("无法解析的mq对象消息:" + message.getClass().getName());
		}
	}
}
