package cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.request.algorithm.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.message.MQMessageOfESM27;
import cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.request.algorithm.service.AlgorithmService;

@RestController
@RequestMapping("/algorithm")
public class AlgorithmController{
	@Autowired
	private AlgorithmService algorithmService;
	/**
	 * 开启推送气体任务
	 * @param request
	 * @param response
	 * @param messageOfESM27
	 * @return
	 */
	@RequestMapping("/StartGasDispersionAlgorithmService")
	public String StartGasDispersionAlgorithmService(HttpServletRequest request,
			HttpServletResponse response,@RequestBody MQMessageOfESM27 t) {
		return algorithmService.StartGasDispersionAlgorithmService(t);
	}
	
	/**
	 * 关闭推送气体任务
	 * @param request
	 * @param response
	 * @param messageOfESM27
	 * @return
	 */
	@RequestMapping(value="CloseGasDispersionAlgorithmService")
	public String CloseGasDispersionAlgorithmService(HttpServletRequest request,
			HttpServletResponse response,@RequestBody MQMessageOfESM27 t) {
		return algorithmService.CloseGasDispersionAlgorithmService(t);
	}
	
	
	/**
	 * 开启推送气体任务
	 * @param request
	 * @param response
	 * @param messageOfESM27
	 * @return
	 */
	@RequestMapping(value="StartParticleLandingAlgorithmService")
	public String StartParticleLandingAlgorithmService(HttpServletRequest request,
			HttpServletResponse response,@RequestBody MQMessageOfESM27 t) {
		return algorithmService.StartParticleLandingAlgorithmService(t);
	}
	
	/**
	 * 关闭推送气体任务
	 * @param request
	 * @param response
	 * @param messageOfESM27
	 * @return
	 */
	@RequestMapping(value="CloseParticleLandingAlgorithmService")
	public String CloseParticleLandingAlgorithmService(HttpServletRequest request,
			HttpServletResponse response,@RequestBody MQMessageOfESM27 t) {
		return algorithmService.CloseParticleLandingAlgorithmService(t);
	}
}
