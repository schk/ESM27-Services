package cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.request.algorithm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.entity.GasAlgorithmManager;
import cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.entity.GasDispersionParamsEntity;
import cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.message.MQMessageOfESM27;
import cn.stronglink.esm27.algorithm.iot.devices.algorithmservice.task.AutoPushGasAlgorithmDatasTask;

@Service("algorithmService")
public class AlgorithmService {
	
	protected final static String SEND_START_GAS_DISPERSION_ACTION_CODE="StartGasDispersionAlgorithmServiceReturn";
	protected final static String SEND_CLOSE_GAS_DISPERSION_ACTION_CODE="CloseGasDispersionAlgorithmServiceReturn";
	protected final static String SEND_START_PARTICLE_LANDING_ACTION_CODE="StartParticleLandingAlgorithmServiceReturn";
	protected final static String SEND_CLOSE_PARTICLE_LANDING_ACTION_CODE="CloseParticleLandingAlgorithmServiceReturn";

	@Autowired
	private AutoPushGasAlgorithmDatasTask autoPushGasAlgorithmDatasTask;
	
	public String StartGasDispersionAlgorithmService(MQMessageOfESM27 t) {
		try {
			GasDispersionParamsEntity gasParams = ((JSONObject)t.getAwsPostdata()).toJavaObject(GasDispersionParamsEntity.class);
			GasAlgorithmManager.setGasDispersionParams(gasParams); // 将数据保存的公共变量中
			// 开启任务
			String strSpanTime = this.time2Cron((int)gasParams.getPushSpanSecond());
			autoPushGasAlgorithmDatasTask.setCron(strSpanTime); 
			return this.returnMessage(SEND_START_GAS_DISPERSION_ACTION_CODE, t.getTimestamp(), true, null);
		}catch(Exception e) {
			e.printStackTrace();
			return this.returnMessage(SEND_START_GAS_DISPERSION_ACTION_CODE, t.getTimestamp(), false, e.getMessage());
		}
	}
	
	
	private String returnMessage(String actionCode,long time,boolean isSuccess,String msg) {
		MQMessageOfESM27 message = new MQMessageOfESM27();
		message.setActioncode(actionCode);
		message.setTimestamp(time);
		message.setAwsPostdata(msg);
		message.setSuccess(isSuccess);
		String json = JSON.toJSONString(message);
		return json;
	}
	
	

	public String CloseGasDispersionAlgorithmService(MQMessageOfESM27 t) {
		try {
			autoPushGasAlgorithmDatasTask.setCron(null); 
			return this.returnMessage(SEND_CLOSE_GAS_DISPERSION_ACTION_CODE, t.getTimestamp(), true, null);
		}catch(Exception e) {
			return this.returnMessage(SEND_CLOSE_GAS_DISPERSION_ACTION_CODE, t.getTimestamp(), true, null);
		}
	}
	
	

	public String StartParticleLandingAlgorithmService(MQMessageOfESM27 t) {
		try {
			GasDispersionParamsEntity gasParams = ((JSONObject)t.getAwsPostdata()).toJavaObject(GasDispersionParamsEntity.class);
			GasAlgorithmManager.setGasDispersionParams(gasParams); // 将数据保存的公共变量中
			// 开启任务
			String strSpanTime = this.time2Cron((int)gasParams.getPushSpanSecond());
			autoPushGasAlgorithmDatasTask.setCron(strSpanTime); 
			return this.returnMessage(SEND_START_PARTICLE_LANDING_ACTION_CODE, t.getTimestamp(), true, null);
		}catch(Exception e) {
			e.printStackTrace();
			return this.returnMessage(SEND_START_PARTICLE_LANDING_ACTION_CODE, t.getTimestamp(), false, e.getMessage());
		}
	}


	public String CloseParticleLandingAlgorithmService(MQMessageOfESM27 t) {
		try {
			autoPushGasAlgorithmDatasTask.setCron(null); 
			return this.returnMessage(SEND_CLOSE_PARTICLE_LANDING_ACTION_CODE, t.getTimestamp(), true, null);
		}catch(Exception e) {
			return this.returnMessage(SEND_CLOSE_PARTICLE_LANDING_ACTION_CODE, t.getTimestamp(), true, null);
		}
	}

	

	/**
	 * 时间转定时规则字符串
	 * @param time 需要定时执行的秒数
	 * @return
	 */
	private String time2Cron(int time) {
		int minute = time/60;
		int seconds = time % 60;
		String cron = "0/" +seconds + " 0/"+minute+ " * * * ?";
		cron = cron.replace("0/0", "*");
		return cron;
	}

}
