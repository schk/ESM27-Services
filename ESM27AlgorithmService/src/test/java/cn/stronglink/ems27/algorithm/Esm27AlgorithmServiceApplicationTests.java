package cn.stronglink.ems27.algorithm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Esm27AlgorithmServiceApplicationTests.class)
public class Esm27AlgorithmServiceApplicationTests {

	@Test
	public void contextLoads() {
	}

}
