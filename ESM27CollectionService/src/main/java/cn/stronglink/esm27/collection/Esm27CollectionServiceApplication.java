package cn.stronglink.esm27.collection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class Esm27CollectionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(Esm27CollectionServiceApplication.class, args);
	}
}
