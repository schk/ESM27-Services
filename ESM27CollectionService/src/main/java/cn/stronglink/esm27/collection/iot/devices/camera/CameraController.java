package cn.stronglink.esm27.collection.iot.devices.camera;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Preconditions;

import cn.stronglink.esm27.collection.iot.devices.camera.entity.CameraParame;
import cn.stronglink.esm27.collection.iot.devices.camera.util.FFMpegUtil;

@CrossOrigin
@RestController()
public class CameraController {
	private final static Logger logger = LogManager.getLogger(CameraController.class);

	@ResponseBody
	@RequestMapping(value = "rtspPush", method = RequestMethod.POST)
	public String rtspPush(@RequestBody JSONObject jsonParam) {
		try {
			List<CameraParame> cameraIds = jsonParam.getJSONArray("cameraIds").toJavaList(CameraParame.class);
			for (CameraParame cp : cameraIds) {
				Preconditions.checkNotNull(cp, "摄像头参数不能为空");
				Preconditions.checkNotNull(cp.getAccount(), "摄像头用户名不能为空");
				Preconditions.checkNotNull(cp.getPassword(), "摄像头密码不能为空");
				Preconditions.checkNotNull(cp.getIp(), "摄像头IP不能为空");
				Preconditions.checkNotNull(cp.getWidth(), "摄像头屏幕宽度不能为空");
				Preconditions.checkNotNull(cp.getHeight(), "摄像头屏幕高度不能为空");
			}
			String steamserver = jsonParam.getString("steamserver");
			FFMpegUtil.rtspPush(cameraIds, steamserver);
			return "{isSuccess:true}";
		} catch (Exception e) {
			logger.error("", e);
			return "{isSuccess:false,message:" + e.getMessage() + "}";
		}
	}
}
