package cn.stronglink.esm27.collection.iot.devices.camera.util;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cc.eguid.FFmpegCommandManager.FFmpegManager;
import cc.eguid.FFmpegCommandManager.FFmpegManagerImpl;
import cn.stronglink.esm27.collection.iot.devices.camera.entity.CameraParame;

public class FFMpegUtil {
	
	private static FFmpegManager manager=new FFmpegManagerImpl(10);
	private final static Logger logger = LogManager.getLogger(FFMpegUtil.class);
	public static void rtspPush(List<CameraParame> cameraIds, String steamserver) throws Exception {

//		Runtime.getRuntime().exec("TASKKILL /F /IM ffmpeg.exe");
		manager.stopAll();
		Thread.sleep(2000);
//		for(CameraSet cs : cameras) {
//			String cmd = "D:\\ffmpeg\\ffmpeg.exe -i rtsp://"+ cs.getIp() +"/h264/ch1/sub/av_stream -f flv -r 25 -s 640x480 -an rtmp://"+ "127.0.0.1:1935" +"/hls/" + Base64EncryptUtil.encrypt(cs.getIp());
//			 Thread thread = new FFMpegThread(cmd);
//		     thread.start();
//		}
//		return true;
		
		for(CameraParame cp : cameraIds) {
			String id=cp.getAccount()+":"+cp.getPassword()+"@"+cp.getIp();
//			String cmd = "d:/ffmpeg/ffmpeg -i rtsp://"+ id + "/h264/ch1/sub/av_stream -f flv -r 25 -s "+cp.getWidth()+"x"+cp.getHeight()+" -an rtmp://"+ steamserver +"/hls/" + Base64EncryptUtil.encrypt(id);
			String encryp = Base64EncryptUtil.encrypt(id).replaceAll("/", "");
			logger.info("视频路径id:{}", id);
			logger.info("视频路径加密id:{}", encryp);
//			String cmd = "d:/ffmpeg/ffmpeg -i rtsp://"+ id + "/h264/ch1/sub/av_stream -f flv -r 25 -vf scale=-1:768 -an rtmp://"+ steamserver +"/hls/" + encryp;
			String cmd = "D:/ffmpeg/ffmpeg -i rtsp://"+ id + "/h264/ch1/sub/av_stream -f flv -r 25 -vf scale=-1:768 -an rtmp://"+ steamserver +"/hls/" + encryp;
			logger.info("开始执行视频开启命令:{}", cmd);
			manager.start(encryp, cmd,true);
			logger.info("视频开启命令执行完成");
		}
	}
	
	public static void main(String[] args) throws Exception {   
//		FFmpegManager manager=new FFmpegManagerImpl(10); 
//		manager.start("1", "d:/ffmpeg/ffmpeg -i rtsp://192.168.2.188/h264/ch1/sub/av_stream -f flv -r 25 -s 640x480 -an rtmp://127.0.0.1:1935/hls/1",true);
		String id="admin:1q2w3e4r@192.168.188.65";
		String encryp = Base64EncryptUtil.encrypt(id).replace("/", "");
		System.out.println(encryp);
		String cmd = "d:/ffmpeg/ffmpeg -i rtsp://"+ id + "/h264/ch1/sub/av_stream -f flv -r 25 -vf scale=-1:768 -an rtmp://hls/" + encryp;
System.out.println(cmd);
	}

}


