package cn.stronglink.esm27.collection.iot.devices.esm27.entity;

/**
 * 报警信息的实体
 * @author yuzhantao
 *
 */
public class AlarmInfoEntity {
	/**
	 * 传感器名称
	 */
	private String sensorName;
	/**
	 * 传感器通道号
	 */
	private int sensorChannelNumber;
	
	/**
	 * 是否报警
	 */
	private boolean isAlarm;

	/**
	 * 报警内容
	 */
	private String alarmContent;
	
	public String getSensorName() {
		return sensorName;
	}

	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}

	public int getSensorChannelNumber() {
		return sensorChannelNumber;
	}

	public void setSensorChannelNumber(int sensorChannelNumber) {
		this.sensorChannelNumber = sensorChannelNumber;
	}

	public boolean isAlarm() {
		return isAlarm;
	}

	public void setAlarm(boolean isAlarm) {
		this.isAlarm = isAlarm;
	}

	public String getAlarmContent() {
		return alarmContent;
	}

	public void setAlarmContent(String alarmContent) {
		this.alarmContent = alarmContent;
	}
}
