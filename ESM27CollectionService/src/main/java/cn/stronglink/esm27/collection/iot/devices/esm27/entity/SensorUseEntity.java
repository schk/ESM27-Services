package cn.stronglink.esm27.collection.iot.devices.esm27.entity;

/**
 * 传感器使能对象
 * @author yuzhantao
 *
 */
public class SensorUseEntity {
	/**
	 * 传感器名称
	 */
	private String name;
	/**
	 * 传感器所在编号
	 */
	private byte channelNumber;
	/**
	 * 是否使用
	 */
	private boolean isUse;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public byte getChannelNumber() {
		return channelNumber;
	}
	public void setChannelNumber(byte channelNumber) {
		this.channelNumber = channelNumber;
	}
	public boolean isUse() {
		return isUse;
	}
	public void setUse(boolean isUse) {
		this.isUse = isUse;
	}
}
