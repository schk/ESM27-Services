package cn.stronglink.esm27.collection.iot.devices.esm27.message;

import java.util.HashMap;
import java.util.Map;

/**
 * 气体相关数据管理类
 * @author yuzhantao
 *
 */
public class GasDataManager {
	private Map<Integer,String> gasNameMap = new HashMap<>();	// 气体名称表
	private Map<Integer,String> gasUnitMap = new HashMap<>();	// 气体单位表
	private Map<Integer,String> gasStateMap = new HashMap<>();  	// 气体状态表
	
	private static GasDataManager instance=new GasDataManager();
	
	/**
	 * 根据气体名称Id查找气体名称
	 * @param gasNameId
	 * @return
	 */
	public static String findGasName(int gasNameId) {
		return instance.gasNameMap.get(gasNameId);
	}
	
	/**
	 * 根据气体单位Id查找气体单位名称
	 * @param gasUnitId
	 * @return
	 */
	public static String findGasUnit(int gasUnitId) {
		return instance.gasUnitMap.get(gasUnitId);
	}
	
	/**
	 * 根据气体状态Id查找气体状态名称
	 * @param gasStateId
	 * @return
	 */
	public static String findGasState(int gasStateId) {
		return instance.gasStateMap.get(gasStateId);
	}
	
	/**
	 * 气体数据管理构造函数
	 */
	private GasDataManager() {
		gasNameMapInit();	// 初始化气体名称信息
		gasUnitInit();		// 初始化气体单位信息
		gasStateInit();		// 初始化气体状态信息
	}
	
	private void gasStateInit() {
		gasStateMap.put(0x01, "正常");
		gasStateMap.put(0x02, "数据错误");
		gasStateMap.put(0x03, "传感器故障");
		gasStateMap.put(0x04, "预警");
		gasStateMap.put(0x05, "低报");
		gasStateMap.put(0x06, "高报");
		gasStateMap.put(0x07, "访问故障");
		gasStateMap.put(0x08, "超量程");
		gasStateMap.put(0x09, "需要标定");
		gasStateMap.put(0x0A, "超时");
		gasStateMap.put(0x0B, "stel报警");
		gasStateMap.put(0x0C, "twa报警");
		gasStateMap.put(0x0F, "通信故障"); 
	}
	
	/**
	 * 气体单位数据初始化
	 */
	private void gasUnitInit() {
		gasUnitMap.put(0x01, "%LEL");
		gasUnitMap.put(0x02, "VOL%");
		gasUnitMap.put(0x03, "PPM");
		gasUnitMap.put(0x04, "mg/m3");
		gasUnitMap.put(0x05, "mg/L");
		gasUnitMap.put(0x06, "%");
		gasUnitMap.put(0x07, "uSv");
		gasUnitMap.put(0x08, "uSv/h");
	}
	
	/**
	 * 气体名称初始化
	 */
	private void gasNameMapInit() {
		gasNameMap.put(0x00, "新类型");
		gasNameMap.put(0x01, "甲烷");
		gasNameMap.put(0x02, "氨气");
		gasNameMap.put(0x03, "硫化氢");
		gasNameMap.put(0x04, "一氧化碳");
		gasNameMap.put(0x05, "氧气");
		gasNameMap.put(0x06, "氢气");
		gasNameMap.put(0x07, "乙烷");
		gasNameMap.put(0x08, "乙烯");
		gasNameMap.put(0x09, "乙炔");
		gasNameMap.put(0x0A, "丙烷");
		gasNameMap.put(0x0B, "丙烯");
		gasNameMap.put(0x0C, "丁烷");
		gasNameMap.put(0x0D, "丁烯");
		gasNameMap.put(0x0E, "丁二烯");
		gasNameMap.put(0x0F, "轻油");
		gasNameMap.put(0x10, "重油");
		gasNameMap.put(0x11, "汽油");
		gasNameMap.put(0x12, "柴油");
		gasNameMap.put(0x13, "煤油");
		gasNameMap.put(0x14, "甲醇");
		gasNameMap.put(0x15, "乙醇");
		gasNameMap.put(0x16, "异丙醇");
		gasNameMap.put(0x17, "甲醛");
		gasNameMap.put(0x18, "丁醛");
		gasNameMap.put(0x19, "丙酮");
		gasNameMap.put(0x1A, "丁酮");
		gasNameMap.put(0x1B, "苯");
		gasNameMap.put(0x1C, "甲苯");
		gasNameMap.put(0x1D, "二甲苯");
		gasNameMap.put(0x1E, "苯乙烯");
		gasNameMap.put(0x1F, "苯酚");
		gasNameMap.put(0x20, "乙醚");
		gasNameMap.put(0x21, "二甲醚");
		gasNameMap.put(0x22, "石油醚");
		gasNameMap.put(0x23, "二甲胺");
		gasNameMap.put(0x24, "三甲胺");
		gasNameMap.put(0x25, "甲酰胺");
		gasNameMap.put(0x26, "四氢呋喃");
		gasNameMap.put(0x27, "醋酸乙酯");
		gasNameMap.put(0x28, "氯代甲苯");
		gasNameMap.put(0x29, "环氧乙烷");
		gasNameMap.put(0x2A, "臭氧");
		gasNameMap.put(0x2B, "二氧化硫");
		gasNameMap.put(0x2C, "二氧化氮");
		gasNameMap.put(0x2D, "一氧化氮");
		gasNameMap.put(0x2E, "氯化氢");
		gasNameMap.put(0x2F, "氰化氢");
		gasNameMap.put(0x30, "二氧化碳");
		gasNameMap.put(0x31, "氯气");
		gasNameMap.put(0x32, "可燃气体");
		gasNameMap.put(0x33, "丙烯腈");
		gasNameMap.put(0x34, "氟化氢");
		gasNameMap.put(0x35, "磷化氢");
		gasNameMap.put(0x36, "二氧化氯");
		gasNameMap.put(0x37, "四氢噻酚");
		gasNameMap.put(0x38, "碘甲烷");
		gasNameMap.put(0x39, "三氯甲烷");
		gasNameMap.put(0x3A, "硅烷");
		gasNameMap.put(0x3B, "氯乙烯");
		gasNameMap.put(0x3C, "光气");
		gasNameMap.put(0x3D, "三氢化砷");
		gasNameMap.put(0x3E, "溴化氢");
		gasNameMap.put(0x3F, "二硫化碳");
		gasNameMap.put(0x40, "环乙烷");
		gasNameMap.put(0x41, "毒性气体");
		gasNameMap.put(0x42, "一甲胺");
		gasNameMap.put(0x43, "甲胺");
		gasNameMap.put(0x44, "DMF");
		gasNameMap.put(0x45, "有机胺");
		gasNameMap.put(0x46, "六氟化氢");
		gasNameMap.put(0x47, "异丁烯");
		gasNameMap.put(0x48, "苯胺");
		gasNameMap.put(0x49, "双氧水");
		gasNameMap.put(0x4A, "双光气");
		gasNameMap.put(0x4B, "三乙胺");
		gasNameMap.put(0x4C, "乙腈");
		gasNameMap.put(0x4D, "硝酸");
		gasNameMap.put(0x4E, "VOC");
		gasNameMap.put(0x4F, "Gamma");
	}
}
