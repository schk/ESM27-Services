package cn.stronglink.esm27.collection.iot.devices.esm27.message;

/**
 * MQ消息实体
 * @author yuzhantao
 *
 */
public class MQMessageOfESM27 {
	/**
	 * 业务编码
	 */
	private String actioncode;
	/**
	 * 业务数据
	 */
	private Object awsPostdata;
	/**
	 * 时间戳
	 */
	private long timeStamp;
	/**
	 * 是否成功
	 */
	private boolean isSuccess;
	/**
	 * 错误编码
	 */
	private String errorCode;
	/**
	 * 错误信息
	 */
	private String errorMessage;
	
	public String getActioncode() {
		return actioncode;
	}
	public void setActioncode(String actioncode) {
		this.actioncode = actioncode;
	}
	public Object getAwsPostdata() {
		return awsPostdata;
	}
	public void setAwsPostdata(Object awsPostdata) {
		this.awsPostdata = awsPostdata;
	}
	public long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}
	public boolean getIsSuccess() {
		return isSuccess;
	}
	public void setIsSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
