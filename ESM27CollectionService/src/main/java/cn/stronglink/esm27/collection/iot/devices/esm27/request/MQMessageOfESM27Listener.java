package cn.stronglink.esm27.collection.iot.devices.esm27.request;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.google.gson.reflect.TypeToken;

import cn.stronglink.esm27.collection.core.message.MessageHandleContext;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.MQMessageOfESM27;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.CallHandle;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.CloseAutoCollectTaskHandle;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.GetAllSensorDataHandle;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.GetDeviceBasePropertyHandle;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.GetDeviceOptionHandle;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.GetDeviceRealtimeDataHandle;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.GetSensorBasePropertyHandle;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.GetSensorOptionHandle;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.GetSystemSerialPortListHandle;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.OpenSerialPortHandle;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.SetDateTimeHandle;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.SetDeviceOptionHandle;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.SetGasOptionHandle;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.handle.SetSensorOptionHandle;

/**
 * ESM27Mq消息监听
 * 
 * @author yuzhantao
 *
 */
@Component
public class MQMessageOfESM27Listener {
	private static Logger logger = LogManager.getLogger(MQMessageOfESM27Listener.class.getName());
	// 通过策略模式处理不同的信息
	private MessageHandleContext<MQMessageOfESM27, Object> messageHandleContent;

	public MQMessageOfESM27Listener() {
		super();
		// 添加处理策略
		this.messageHandleContent = new MessageHandleContext<>();
		this.messageHandleContent.addHandleClass(new CallHandle());
		this.messageHandleContent.addHandleClass(new GetAllSensorDataHandle());
		this.messageHandleContent.addHandleClass(new GetDeviceBasePropertyHandle());
		this.messageHandleContent.addHandleClass(new GetDeviceOptionHandle());
		this.messageHandleContent.addHandleClass(new GetDeviceRealtimeDataHandle());
		this.messageHandleContent.addHandleClass(new GetSensorBasePropertyHandle());
		this.messageHandleContent.addHandleClass(new GetSensorOptionHandle());
		this.messageHandleContent.addHandleClass(new SetDateTimeHandle());
		this.messageHandleContent.addHandleClass(new SetDeviceOptionHandle());
		this.messageHandleContent.addHandleClass(new SetGasOptionHandle());
		this.messageHandleContent.addHandleClass(new SetSensorOptionHandle());
//		this.messageHandleContent.addHandleClass(new OpenSerialPortHandle());
		this.messageHandleContent.addHandleClass(new GetSystemSerialPortListHandle());
//		this.messageHandleContent.addHandleClass(new CloseAutoCollectTaskHandle());
	}

	@JmsListener(destination = "ServiceToESM27", containerFactory = "jmsListenerServer")
	public void onMessage(Message message) throws JMSException {
		try {
			if (message instanceof TextMessage) {
				TextMessage tm = (TextMessage) message;
				String json = tm.getText();
				MQMessageOfESM27 rm = JSON.parseObject(json, new TypeToken<MQMessageOfESM27>() {
				}.getType());
				this.messageHandleContent.handle(null, rm);
			} else {
				logger.error("无法解析的mq对象消息:" + message.getClass().getName());
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}
	

	@Bean
	public JmsListenerContainerFactory<?> jmsListenerServer(ConnectionFactory activeMQConnectionFactory) {
		DefaultJmsListenerContainerFactory bean = new DefaultJmsListenerContainerFactory();
		bean.setPubSubDomain(true);
		bean.setConnectionFactory(activeMQConnectionFactory);
		return bean;
	}
}
