package cn.stronglink.esm27.collection.iot.devices.esm27.request.entity;

/**
 * 设备信息
 * @author yuzhantao
 *
 */
public class DeviceEntity {
	private byte deviceAddress;

	public byte getDeviceAddress() {
		return deviceAddress;
	}

	public void setDeviceAddress(byte deviceAddress) {
		this.deviceAddress = deviceAddress;
	}

	
}
