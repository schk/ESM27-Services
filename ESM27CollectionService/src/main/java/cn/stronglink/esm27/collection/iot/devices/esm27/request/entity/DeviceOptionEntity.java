package cn.stronglink.esm27.collection.iot.devices.esm27.request.entity;

/**
 * 设备设置实体
 * @author yuzhantao
 *
 */
public class DeviceOptionEntity extends DeviceEntity {
	/**
	 * 传感器的数量
	 */
	private byte sensorCount;
	
	/**
	 * 设备使能标志(传感器、北斗、无线均使能)
	 */
	private short deviceUse;
	
	/**
	 * 无线通讯通道
	 */
	private byte wirelessCommunicationChannel;
	
	/**
	 * 无线通讯速率
	 */
	private byte wirelessCommunicationRate;
	
	/**
	 * 北斗刷新周期
	 */
	private short beidouRefreshCycle;
	
	/**
	 * 报警使能
	 */
	private int alarmOption;
	
	/**
	 * 电池欠压阀值
	 */
	private float batteryUndervoltageThreshold;
	
	/**
	 * 电池欠压关机阀值
	 */
	private float batteryUndervoltageShutdownThreshold;
	
	/**
	 * 背光延迟时间
	 */
	private byte backlightDelayTime;
	
	/**
	 * 背光模式
	 */
	private byte backlightMode;
	
	/**
	 * 泵周期
	 */
	private byte pumpCycle;
	
	/**
	 * 泵占空比
	 */
	private byte pumpDutyRatio;
	
	/**
	 * 当前时间
	 */
	private long currentTime;

	public byte getSensorCount() {
		return sensorCount;
	}

	public void setSensorCount(byte sensorCount) {
		this.sensorCount = sensorCount;
	}

	public short getDeviceUse() {
		return deviceUse;
	}

	public void setDeviceUse(short deviceUse) {
		this.deviceUse = deviceUse;
	}

	public byte getWirelessCommunicationChannel() {
		return wirelessCommunicationChannel;
	}

	public void setWirelessCommunicationChannel(byte wirelessCommunicationChannel) {
		this.wirelessCommunicationChannel = wirelessCommunicationChannel;
	}

	public byte getWirelessCommunicationRate() {
		return wirelessCommunicationRate;
	}

	public void setWirelessCommunicationRate(byte wirelessCommunicationRate) {
		this.wirelessCommunicationRate = wirelessCommunicationRate;
	}

	public short getBeidouRefreshCycle() {
		return beidouRefreshCycle;
	}

	public void setBeidouRefreshCycle(short beidouRefreshCycle) {
		this.beidouRefreshCycle = beidouRefreshCycle;
	}

	public int getAlarmOption() {
		return alarmOption;
	}

	public void setAlarmOption(int alarmOption) {
		this.alarmOption = alarmOption;
	}

	public float getBatteryUndervoltageThreshold() {
		return batteryUndervoltageThreshold;
	}

	public void setBatteryUndervoltageThreshold(float batteryUndervoltageThreshold) {
		this.batteryUndervoltageThreshold = batteryUndervoltageThreshold;
	}

	public float getBatteryUndervoltageShutdownThreshold() {
		return batteryUndervoltageShutdownThreshold;
	}

	public void setBatteryUndervoltageShutdownThreshold(float batteryUndervoltageShutdownThreshold) {
		this.batteryUndervoltageShutdownThreshold = batteryUndervoltageShutdownThreshold;
	}

	public byte getBacklightDelayTime() {
		return backlightDelayTime;
	}

	public void setBacklightDelayTime(byte backlightDelayTime) {
		this.backlightDelayTime = backlightDelayTime;
	}

	public byte getBacklightMode() {
		return backlightMode;
	}

	public void setBacklightMode(byte backlightMode) {
		this.backlightMode = backlightMode;
	}

	public byte getPumpCycle() {
		return pumpCycle;
	}

	public void setPumpCycle(byte pumpCycle) {
		this.pumpCycle = pumpCycle;
	}

	public byte getPumpDutyRatio() {
		return pumpDutyRatio;
	}

	public void setPumpDutyRatio(byte pumpDutyRatio) {
		this.pumpDutyRatio = pumpDutyRatio;
	}

	public long getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(long currentTime) {
		this.currentTime = currentTime;
	}
	
	
}
