package cn.stronglink.esm27.collection.iot.devices.esm27.request.entity;

import cn.stronglink.esm27.collection.core.util.ByteUtil;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.ESM27MessageTools;

/**
 * ESM27请求信息制造工厂类
 * @author yuzhantao
 *
 */
public class ESM27RequestMessageFactory {
	public static byte[] createMessage(byte deviceAddress,byte commandCode,byte[] datas) {
		int dataLen = 0;
		if(datas!=null) {
			dataLen=datas.length;
		}
		
		byte[] retDatas = new byte[dataLen+5];
		retDatas[0]=deviceAddress;
		retDatas[1]=commandCode;
		retDatas[2]=((Integer)dataLen).byteValue();
		if(datas!=null) {
			System.arraycopy(datas, 0, retDatas, 3, dataLen);
		}
		System.arraycopy(ByteUtil.shortToByteArr(ESM27MessageTools.calcCRC(retDatas,0,retDatas.length-2)), 0, retDatas, retDatas.length-2, 2);

		return retDatas;
	}
}
