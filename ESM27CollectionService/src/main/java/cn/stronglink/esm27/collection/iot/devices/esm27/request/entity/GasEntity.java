package cn.stronglink.esm27.collection.iot.devices.esm27.request.entity;

/**
 * 气体中包含的参数
 * @author yuzhantao
 *
 */
public class GasEntity {
	/**
	 * 是否可以使用
	 */
	private boolean isUse;
	/**
	 * 气体类型的编号
	 */
	private byte gasTypeId;
	/**
	 * 收集气体的传感器编号
	 */
	private int sensorChannelNumber;
	public boolean isUse() {
		return isUse;
	}
	public void setUse(boolean isUse) {
		this.isUse = isUse;
	}
	public byte getGasTypeId() {
		return gasTypeId;
	}
	public void setGasTypeId(byte gasTypeId) {
		this.gasTypeId = gasTypeId;
	}
	public int getSensorChannelNumber() {
		return sensorChannelNumber;
	}
	public void setSensorChannelNumber(int sensorChannelNumber) {
		this.sensorChannelNumber = sensorChannelNumber;
	}
	
}
