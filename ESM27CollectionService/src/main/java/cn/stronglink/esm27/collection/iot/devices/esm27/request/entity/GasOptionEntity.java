package cn.stronglink.esm27.collection.iot.devices.esm27.request.entity;

import java.util.List;

/**
 * 气体设置的实例
 * @author yuzhantao
 *
 */
public class GasOptionEntity extends DeviceEntity {
	private List<GasEntity> gasList;

	public List<GasEntity> getGasList() {
		return gasList;
	}

	public void setGasList(List<GasEntity> gasList) {
		this.gasList = gasList;
	}
}
