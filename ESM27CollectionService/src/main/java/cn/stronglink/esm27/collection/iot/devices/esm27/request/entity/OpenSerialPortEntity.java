package cn.stronglink.esm27.collection.iot.devices.esm27.request.entity;

import java.util.List;

public class OpenSerialPortEntity {
	/**
	 * 串口名
	 */
	String serialPortName;
	
	/**
	 * 主动上传传感器数据的好描数
	 */
	int autoPushSensorDataIntervalSeconds;
	/**
	 * 用户Id
	 */
	String userId;

	/**
	 * 聊天室Id
	 */
	String roomId;
	
	List<Integer> deviceIds;
	
	public String getSerialPortName() {
		return serialPortName;
	}

	public void setSerialPortName(String serialPortName) {
		this.serialPortName = serialPortName;
	}

	public int getAutoPushSensorDataIntervalSeconds() {
		return autoPushSensorDataIntervalSeconds;
	}

	public void setAutoPushSensorDataIntervalSeconds(int autoPushSensorDataIntervalSeconds) {
		this.autoPushSensorDataIntervalSeconds = autoPushSensorDataIntervalSeconds;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public List<Integer> getDeviceIds() {
		return deviceIds;
	}

	public void setDeviceIds(List<Integer> deviceIds) {
		this.deviceIds = deviceIds;
	}

}
