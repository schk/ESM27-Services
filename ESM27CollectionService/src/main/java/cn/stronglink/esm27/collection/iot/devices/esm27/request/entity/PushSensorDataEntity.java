package cn.stronglink.esm27.collection.iot.devices.esm27.request.entity;
/**
 * 推送传感器数据的提示
 * @author yuzhantao
 *
 */
public class PushSensorDataEntity {
	private OpenSerialPortEntity openSerialPortEntity;

	private static PushSensorDataEntity instance;
	
	public static PushSensorDataEntity getInstance() {
		if(instance==null) {
			instance=new PushSensorDataEntity();
		}
		
		return instance;
	}
	
	private PushSensorDataEntity() {}

	public OpenSerialPortEntity getOpenSerialPortEntity() {
		return openSerialPortEntity;
	}

	public void setOpenSerialPortEntity(OpenSerialPortEntity openSerialPortEntity) {
		this.openSerialPortEntity = openSerialPortEntity;
	}
	
	
}
