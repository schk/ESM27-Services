package cn.stronglink.esm27.collection.iot.devices.esm27.request.entity;

/**
 * 传感器基础属性实体
 * @author yuzhantao
 *
 */
public class SensorBasePropertyEntity extends DeviceEntity{
	/**
	 * 传感器的通道号
	 */
	private int sensorChannelNumber;

	public int getSensorChannelNumber() {
		return sensorChannelNumber;
	}

	public void setSensorChannelNumber(int sensorChannelNumber) {
		this.sensorChannelNumber = sensorChannelNumber;
	}

}
