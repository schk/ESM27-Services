package cn.stronglink.esm27.collection.iot.devices.esm27.request.entity;

/**
 * 传感器设置的实例
 * @author yuzhantao
 *
 */
public class SensorOptionEntity extends DeviceEntity {
	/**
	 * 感器通道号=00传感器模块1（0～6表示传感器1～7）
	 */
	private byte sensorChannelNumber;

	/**
	 * 低报警点
	 */
	private float minAlarmThreshold;
	/**
	 * 高报警点
	 */
	private float maxAlarmThreshold;
	
	/**
	 * STEL报警点
	 */
	private float STELAlarmThreshold;
	
	/**
	 * TWA报警点
	 */
	private float TWAAlarmThreshold;
	
	/**
	 * 增益
	 */
	private float gain;
	/**
	 * 零点AD
	 */
	private short ZeroPointAD;
	/**
	 * 系统时间
	 */
	private long systemTime;
	

	public byte getSensorChannelNumber() {
		return sensorChannelNumber;
	}
	public void setSensorChannelNumber(byte sensorChannelNumber) {
		this.sensorChannelNumber = sensorChannelNumber;
	}
	public float getMinAlarmThreshold() {
		return minAlarmThreshold;
	}
	public void setMinAlarmThreshold(float minAlarmThreshold) {
		this.minAlarmThreshold = minAlarmThreshold;
	}
	public float getMaxAlarmThreshold() {
		return maxAlarmThreshold;
	}
	public void setMaxAlarmThreshold(float maxAlarmThreshold) {
		this.maxAlarmThreshold = maxAlarmThreshold;
	}
	public float getSTELAlarmThreshold() {
		return STELAlarmThreshold;
	}
	public void setSTELAlarmThreshold(float sTELAlarmThreshold) {
		STELAlarmThreshold = sTELAlarmThreshold;
	}
	public float getTWAAlarmThreshold() {
		return TWAAlarmThreshold;
	}
	public void setTWAAlarmThreshold(float tWAAlarmThreshold) {
		TWAAlarmThreshold = tWAAlarmThreshold;
	}
	public float getGain() {
		return gain;
	}
	public void setGain(float gain) {
		this.gain = gain;
	}
	public short getZeroPointAD() {
		return ZeroPointAD;
	}
	public void setZeroPointAD(short zeroPointAD) {
		ZeroPointAD = zeroPointAD;
	}
	public long getSystemTime() {
		return systemTime;
	}
	public void setSystemTime(long systemTime) {
		this.systemTime = systemTime;
	}

	
}
