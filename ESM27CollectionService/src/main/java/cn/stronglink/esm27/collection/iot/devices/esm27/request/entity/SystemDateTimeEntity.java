package cn.stronglink.esm27.collection.iot.devices.esm27.request.entity;

public class SystemDateTimeEntity  extends DeviceEntity {
	/**
	 * 同步的时间
	 */
	private long syncDateTime;

	public long getSyncDateTime() {
		return syncDateTime;
	}

	public void setSyncDateTime(long syncDateTime) {
		this.syncDateTime = syncDateTime;
	}
	
	
}
