package cn.stronglink.esm27.collection.iot.devices.esm27.request.handle;

import com.alibaba.fastjson.JSONObject;

import cn.stronglink.esm27.collection.core.util.ByteUtil;
import cn.stronglink.esm27.collection.core.util.ContextUtils;
import cn.stronglink.esm27.collection.iot.devices.esm27.ESM27Server;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.MQMessageOfESM27;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.DeviceEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.ESM27RequestMessageFactory;

/**
 * 发送空操作，用于查看设备是否存在
 * @author yuzhantao
 *
 */
public class CallHandle extends AbstractHandle {
	private final static String ACTION_CODE = "Call"; // MQ中判断消息类型的标识符
	private ESM27Server esm27Server=(ESM27Server) ContextUtils.getBean("esm27Server"); // esm27服务器

	@Override
	protected String getActionCode() {
		return ACTION_CODE;
	}

	@Override
	protected void handleMessage(MQMessageOfESM27 t) throws Exception {
		DeviceEntity device = ((JSONObject)t.getAwsPostdata()).toJavaObject(DeviceEntity.class);
		byte[] sendDatas = ESM27RequestMessageFactory.createMessage(
				device.getDeviceAddress(),
				(byte)0x0, 
				null);
		esm27Server.send(sendDatas);
		logger.info("发送消息到ESM27:{}", ByteUtil.byteArrToHexString(sendDatas, true));
	}

}
