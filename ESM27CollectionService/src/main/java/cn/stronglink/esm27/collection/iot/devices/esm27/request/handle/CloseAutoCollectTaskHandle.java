package cn.stronglink.esm27.collection.iot.devices.esm27.request.handle;

import cn.stronglink.esm27.collection.core.util.ContextUtils;
import cn.stronglink.esm27.collection.iot.devices.esm27.ESM27Server;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.MQMessageOfESM27;
import cn.stronglink.esm27.collection.iot.devices.esm27.task.AutoPushSensorDataTask;

public class CloseAutoCollectTaskHandle extends AbstractHandle {
	protected final static String ACTION_CODE = "CloseAutoCollectTask"; // 关闭自动采集任务的标识符
	protected AutoPushSensorDataTask autoPushSensorDataTask = (AutoPushSensorDataTask) ContextUtils
			.getBean("autoPushSensorDataTask");
	private ESM27Server esm27Server = (ESM27Server) ContextUtils.getBean("esm27Server"); // esm27服务器

	@Override
	protected String getActionCode() {
		return ACTION_CODE;
	}

	@Override
	protected void handleMessage(MQMessageOfESM27 t) throws Exception {
		autoPushSensorDataTask.setCron(null);
		esm27Server.closeSerialPort();
		this.sendReturnMessage(null);    // 发送反馈信息
	}
}