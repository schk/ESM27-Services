package cn.stronglink.esm27.collection.iot.devices.esm27.request.handle;

import java.util.List;

import cn.stronglink.esm27.collection.core.util.ContextUtils;
import cn.stronglink.esm27.collection.iot.devices.esm27.ESM27Server;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.MQMessageOfESM27;

/**
 * 获取系统串口列表
 * @author yuzhantao
 *
 */
public class GetSystemSerialPortListHandle extends AbstractHandle {
	protected final static String ACTION_CODE = "GetSystemSerialPortList"; // MQ中判断消息类型的标识符
	protected ESM27Server esm27Server=(ESM27Server) ContextUtils.getBean("esm27Server"); // esm27服务器

	@Override
	protected String getActionCode() {
		return ACTION_CODE;
	}

	@Override
	protected void handleMessage(MQMessageOfESM27 t) throws Exception {
		List<String> serialPortList = esm27Server.getSerialPortList();
		this.sendReturnMessage(serialPortList);
	}
}
