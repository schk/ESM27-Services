package cn.stronglink.esm27.collection.iot.devices.esm27.request.handle;

import java.io.IOException;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSONObject;

import cn.stronglink.esm27.collection.core.message.IMessageHandle;
import cn.stronglink.esm27.collection.core.util.ByteUtil;
import cn.stronglink.esm27.collection.core.util.ContextUtils;
import cn.stronglink.esm27.collection.iot.devices.esm27.ESM27Server;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.MQMessageOfESM27;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.ESM27RequestMessageFactory;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.SystemDateTimeEntity;
import io.netty.channel.ChannelHandlerContext;

/**
 * 设置日期时间
 * @author yuzhantao
 *
 */
public class SetDateTimeHandle implements IMessageHandle<MQMessageOfESM27, Object> {
	private static Logger logger = LogManager.getLogger(SetDateTimeHandle.class.getName());
	private final static String ACTION_CODE = "SetDateTime"; // MQ中判断消息类型的标识符
	private ESM27Server esm27Server=(ESM27Server) ContextUtils.getBean("esm27Server"); // esm27服务器
	
	@Override
	public boolean isHandle(MQMessageOfESM27 t) {
		if (t.getActioncode().equals(ACTION_CODE)) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfESM27 t) {
		try {
			SystemDateTimeEntity sysTime = ((JSONObject)t.getAwsPostdata()).toJavaObject(SystemDateTimeEntity.class);
			
			byte[] timeDatas = new byte[6];
			Date sycnTime = new Date(sysTime.getSyncDateTime());
			timeDatas[0]=((Integer)sycnTime.getYear()).byteValue();
			timeDatas[1]=((Integer)sycnTime.getMonth()).byteValue();
			timeDatas[2]=((Integer)sycnTime.getDate()).byteValue();
			timeDatas[3]=((Integer)sycnTime.getHours()).byteValue();
			timeDatas[4]=((Integer)sycnTime.getMinutes()).byteValue();
			timeDatas[5]=((Integer)sycnTime.getSeconds()).byteValue();
			
			byte[] sendDatas = ESM27RequestMessageFactory.createMessage(
					sysTime.getDeviceAddress(),
					(byte)0x06,
					timeDatas);
			esm27Server.send(sendDatas);
			logger.info("发送消息到ESM27:{}", ByteUtil.byteArrToHexString(sendDatas, true));
		} catch (IOException e) {
			logger.error(e);
		}
		return null;
	}

}
