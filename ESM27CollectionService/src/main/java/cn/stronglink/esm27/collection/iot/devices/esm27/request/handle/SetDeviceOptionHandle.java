package cn.stronglink.esm27.collection.iot.devices.esm27.request.handle;

import java.io.IOException;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSONObject;

import cn.stronglink.esm27.collection.core.message.IMessageHandle;
import cn.stronglink.esm27.collection.core.util.ByteUtil;
import cn.stronglink.esm27.collection.core.util.ContextUtils;
import cn.stronglink.esm27.collection.iot.devices.esm27.ESM27Server;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.MQMessageOfESM27;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.DeviceOptionEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.ESM27RequestMessageFactory;
import io.netty.channel.ChannelHandlerContext;

public class SetDeviceOptionHandle implements IMessageHandle<MQMessageOfESM27, Object> {
	private static Logger logger = LogManager.getLogger(SetDeviceOptionHandle.class.getName());
	private final static String ACTION_CODE = "SetDeviceOption"; // MQ中判断消息类型的标识符
	private ESM27Server esm27Server=(ESM27Server) ContextUtils.getBean("esm27Server"); // esm27服务器
	
	@Override
	public boolean isHandle(MQMessageOfESM27 t) {
		if (t.getActioncode().equals(ACTION_CODE)) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfESM27 t) {
		try {
			DeviceOptionEntity option = ((JSONObject)t.getAwsPostdata()).toJavaObject(DeviceOptionEntity.class);
			byte[] retData = new byte[30];
			// 设置传感器数量
			retData[0]=option.getSensorCount();
			// 设置设备地址
			retData[1]=option.getDeviceAddress();
			// 设置设备使能标志
			System.arraycopy(ByteUtil.shortToByteArr(option.getDeviceUse()), 0, retData, 2, 2);
			// 设置无线通信信道
			retData[4]=option.getWirelessCommunicationChannel();
			// 设置无线通信速率
			retData[5]=option.getWirelessCommunicationRate();
			// 设置北斗刷新周期
			System.arraycopy(ByteUtil.shortToByteArr(option.getBeidouRefreshCycle()), 0, retData, 6, 2);
			// 设置报警使能
			System.arraycopy(ByteUtil.intToByteArray(option.getAlarmOption()), 0, retData, 8, 4);
			// 设置电池欠压阀值
			System.arraycopy(ByteUtil.floatToByteArray(option.getBatteryUndervoltageThreshold()), 0, retData, 12, 4);
			// 设置电池欠压关机阀值
			System.arraycopy(ByteUtil.floatToByteArray(option.getBatteryUndervoltageShutdownThreshold()), 0, retData, 16, 4);
			// 设置背光延时时间
			retData[20]=option.getBacklightDelayTime();
			// 设置背光模式
			retData[21]=option.getBacklightMode();
			// 设置泵周期
			retData[22]=option.getPumpCycle();
			// 设置泵占空比
			retData[23]=option.getPumpDutyRatio();
			// 设置当前时间
			Date sycnTime = new Date(option.getCurrentTime());
			retData[24]=((Integer)sycnTime.getYear()).byteValue();
			retData[25]=((Integer)sycnTime.getMonth()).byteValue();
			retData[26]=((Integer)sycnTime.getDate()).byteValue();
			retData[27]=((Integer)sycnTime.getHours()).byteValue();
			retData[28]=((Integer)sycnTime.getMinutes()).byteValue();
			retData[29]=((Integer)sycnTime.getSeconds()).byteValue();
			
			byte[] sendDatas = ESM27RequestMessageFactory.createMessage(
					option.getDeviceAddress(),
					(byte)0x07,
					retData);
			esm27Server.send(sendDatas);
			logger.info("发送消息到ESM27:{}", ByteUtil.byteArrToHexString(sendDatas, true));
		} catch (IOException e) {
			logger.error(e);
		}
		return null;
	}

}

