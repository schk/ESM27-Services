package cn.stronglink.esm27.collection.iot.devices.esm27.request.handle;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSONObject;

import cn.stronglink.esm27.collection.core.message.IMessageHandle;
import cn.stronglink.esm27.collection.core.util.ByteUtil;
import cn.stronglink.esm27.collection.core.util.ContextUtils;
import cn.stronglink.esm27.collection.iot.devices.esm27.ESM27Server;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.MQMessageOfESM27;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.ESM27RequestMessageFactory;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.GasEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.GasOptionEntity;
import io.netty.channel.ChannelHandlerContext;

/**
 * 设置气体
 * @author yuzhantao
 *
 */
public class SetGasOptionHandle implements IMessageHandle<MQMessageOfESM27, Object> {
	private static Logger logger = LogManager.getLogger(SetGasOptionHandle.class.getName());
	private final static String ACTION_CODE = "SetGasOption"; // MQ中判断消息类型的标识符
	private ESM27Server esm27Server=(ESM27Server) ContextUtils.getBean("esm27Server"); // esm27服务器

	@Override
	public boolean isHandle(MQMessageOfESM27 t) {
		if (t.getActioncode().equals(ACTION_CODE)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfESM27 t) {
		try {
			GasOptionEntity option = ((JSONObject) t.getAwsPostdata()).toJavaObject(GasOptionEntity.class);
			byte[] datas = new byte[option.getGasList().size()*2];
			for(int i=0;i<option.getGasList().size();i++) {
				GasEntity gas = option.getGasList().get(i);
				int index = gas.getSensorChannelNumber()-1;
				datas[index*2]=gas.isUse()?(byte)1:0;
				datas[index*2+1]=gas.getGasTypeId();
			}

			byte[] sendDatas = ESM27RequestMessageFactory
					.createMessage(option.getDeviceAddress(), (byte) 0x09, datas);
			esm27Server.send(sendDatas);
			logger.info("发送消息到ESM27:{}", ByteUtil.byteArrToHexString(sendDatas, true));
		} catch (IOException e) {
			logger.error(e);
		}
		return null;
	}

}
