package cn.stronglink.esm27.collection.iot.devices.esm27.request.handle;

import java.io.IOException;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSONObject;

import cn.stronglink.esm27.collection.core.message.IMessageHandle;
import cn.stronglink.esm27.collection.core.util.ByteUtil;
import cn.stronglink.esm27.collection.core.util.ContextUtils;
import cn.stronglink.esm27.collection.iot.devices.esm27.ESM27Server;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.MQMessageOfESM27;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.ESM27RequestMessageFactory;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.SensorOptionEntity;
import io.netty.channel.ChannelHandlerContext;

/**
 * 设置传感器配置的处理
 * 
 * @author yuzhantao
 *
 */
public class SetSensorOptionHandle implements IMessageHandle<MQMessageOfESM27, Object> {
	private static Logger logger = LogManager.getLogger(SetSensorOptionHandle.class.getName());
	private final static String ACTION_CODE = "SetSensorOption"; // MQ中判断消息类型的标识符
	private ESM27Server esm27Server=(ESM27Server) ContextUtils.getBean("esm27Server"); // esm27服务器

	@Override
	public boolean isHandle(MQMessageOfESM27 t) {
		if (t.getActioncode().equals(ACTION_CODE)) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public Object handle(ChannelHandlerContext ctx, MQMessageOfESM27 t) {
		try {
			SensorOptionEntity option = ((JSONObject) t.getAwsPostdata()).toJavaObject(SensorOptionEntity.class);
			byte[] datas = new byte[29];
			// 设置传感器通道号
			datas[0] = option.getSensorChannelNumber();
			// 设置低报警点
			System.arraycopy(ByteUtil.floatToByteArray(option.getMinAlarmThreshold()), 0, datas, 1, 4);
			// 设置高报警点
			System.arraycopy(ByteUtil.floatToByteArray(option.getMaxAlarmThreshold()), 0, datas, 5, 4);
			// 设置STEL报警点
			System.arraycopy(ByteUtil.floatToByteArray(option.getSTELAlarmThreshold()), 0, datas, 9, 4);
			// 设置TWA报警点
			System.arraycopy(ByteUtil.floatToByteArray(option.getTWAAlarmThreshold()), 0, datas, 13, 4);
			// 设置增益
			System.arraycopy(ByteUtil.floatToByteArray(option.getGain()), 0, datas, 17, 4);
			// 设置零点AD
			System.arraycopy(ByteUtil.shortToByteArr(option.getZeroPointAD()), 0, datas, 21, 2);
			// 设置当前时间
			Date sycnTime = new Date(option.getSystemTime());
			datas[23] = ((Integer) sycnTime.getYear()).byteValue();
			datas[24] = ((Integer) sycnTime.getMonth()).byteValue();
			datas[25] = ((Integer) sycnTime.getDate()).byteValue();
			datas[26] = ((Integer) sycnTime.getHours()).byteValue();
			datas[27] = ((Integer) sycnTime.getMinutes()).byteValue();
			datas[28] = ((Integer) sycnTime.getSeconds()).byteValue();

			byte[] sendDatas = ESM27RequestMessageFactory
					.createMessage(option.getDeviceAddress(), (byte) 0x08, datas);
			esm27Server.send(sendDatas);
			logger.info("发送消息到ESM27:{}", ByteUtil.byteArrToHexString(sendDatas, true));
		} catch (IOException e) {
			logger.error(e);
		}
		return null;
	}

}
