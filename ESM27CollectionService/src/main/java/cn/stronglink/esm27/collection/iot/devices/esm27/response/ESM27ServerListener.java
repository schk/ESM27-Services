package cn.stronglink.esm27.collection.iot.devices.esm27.response;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import cn.stronglink.esm27.collection.core.message.MessageHandleContext;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.coder.ESM27Coder;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.entity.ESM27ResponseMessageEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.handle.*;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

public class ESM27ServerListener implements SerialPortEventListener {

    private InputStream inputStream;

    public byte[] readBuffer = new byte[1024];
    private ByteBuffer buffer = ByteBuffer.allocate(1024);
    private ESM27Coder coder = new ESM27Coder();
    private MessageHandleContext<ESM27ResponseMessageEntity, Object> handleContext = new MessageHandleContext<>();

    public ESM27ServerListener(Integer protocolVersion) {
        System.out.println("气象设备协议版本:" + protocolVersion);
        if (protocolVersion == 1) {
            handleContext.addHandleClass(new GetWindRealtimeDatasReturnHandle());
        } else {
            handleContext.addHandleClass(new GetWindRealtimeDatasReturnHandle2());
        }
        handleContext.addHandleClass(new AutoAlarmHandle());        // 自动上传报警的处理
        handleContext.addHandleClass(new CallReturnHandle());        // 呼叫设备的处理
        handleContext.addHandleClass(new GetAllSensorDataReturnHandle());            // 获取所有传感器数据
        handleContext.addHandleClass(new GetDeviceBasePropertyReturnHandle());        // 获取设备基本属性的反馈处理
        handleContext.addHandleClass(new GetDeviceOptionReturnHandle());            // 获取设备设置信息的反馈处理
        handleContext.addHandleClass(new GetDeviceRealtimeDataReturnHandle());        // 获取设备实时数据
        handleContext.addHandleClass(new GetSensorBasePropertyReturnHandle());        // 获取传感器基本属性的反馈处理
        handleContext.addHandleClass(new GetSensorOptionReturnHandle());            // 获取传感器设置信息的反馈处理
        handleContext.addHandleClass(new SetDateTimeReturnHandle());                // 设置时间的反馈处理
        handleContext.addHandleClass(new SetDeviceOptionReturnHandle());            // 设置设备选项的反馈处理
        handleContext.addHandleClass(new SetGasOptionReturnHandle());                // 设置传感器配置的反馈处理
        handleContext.addHandleClass(new SetSensorOptionReturnHandle());            // 设置传感器配置的反馈处理
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    @Override
    public void serialEvent(SerialPortEvent event) {
        switch (event.getEventType()) {
            case SerialPortEvent.BI:
            case SerialPortEvent.OE:
            case SerialPortEvent.FE:
            case SerialPortEvent.PE:
            case SerialPortEvent.CD:
            case SerialPortEvent.CTS:
            case SerialPortEvent.DSR:
            case SerialPortEvent.RI:
            case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
                break;
            case SerialPortEvent.DATA_AVAILABLE://获取到串口返回信息
                int len = 0;
                do {
                    try {
                        len = inputStream.read(readBuffer);
//                    System.out.println("接收到串口数据:"+ByteUtil.byteArrToHexString(readBuffer));
                        buffer.put(readBuffer, 0, len);
                        Object obj = coder.encoder(buffer);
                        if (obj != null && obj instanceof ESM27ResponseMessageEntity) {
                            handleContext.handle(null, (ESM27ResponseMessageEntity) obj);
                        }
                    } catch (IOException e) {
                        return;
                    }
                }
                while (len > 0);
//            System.out.println("串口关闭");
//            serialPort.close();//这里一定要用close()方法关闭串口，释放资源  
                break;
            default:
                break;
        }
    }

}
