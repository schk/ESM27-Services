package cn.stronglink.esm27.collection.iot.devices.esm27.response.entity;

/**
 * 报警设置实体
 * @author yuzhantao
 *
 */
public class AlarmOptionEntity {
	/**
	 * 传感器名称
	 */
	private String sensorName;
	/**
	 * 传感器通道号
	 */
	private int sensorChannelNumber;
	/**
	 * 报警内容
	 */
	private String alarmContent;
	/**
	 * 是否使能报警
	 */
	private boolean enable;
	public String getSensorName() {
		return sensorName;
	}
	public void setSensorName(String sensorName) {
		this.sensorName = sensorName;
	}
	public int getSensorChannelNumber() {
		return sensorChannelNumber;
	}
	public void setSensorChannelNumber(int sensorChannelNumber) {
		this.sensorChannelNumber = sensorChannelNumber;
	}
	
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	public String getAlarmContent() {
		return alarmContent;
	}
	public void setAlarmContent(String alarmContent) {
		this.alarmContent = alarmContent;
	}
	
	
}
