package cn.stronglink.esm27.collection.iot.devices.esm27.response.entity;

import java.util.List;

import cn.stronglink.esm27.collection.iot.devices.esm27.entity.AlarmInfoEntity;

/**
 * 自动报警实体
 * @author yuzhantao
 *
 */
public class AutoAlarmEntity extends DeviceEntity {
	
	private String userId;
	
	private String roomId;
	
	private List<AlarmInfoEntity> alarmInfoList;

	public List<AlarmInfoEntity> getAlarmInfoList() {
		return alarmInfoList;
	}

	public void setAlarmInfoList(List<AlarmInfoEntity> alarmInfoList) {
		this.alarmInfoList = alarmInfoList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	
}
