package cn.stronglink.esm27.collection.iot.devices.esm27.response.entity;

/**
 * 设备基本属性
 * @author yuzhantao
 *
 */
public class DeviceBasePropertyEntity extends DeviceEntity {
	private String deviceId;
	
	/**
	 * 版本号
	 */
	private int version;
	
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	
}
