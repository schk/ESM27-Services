package cn.stronglink.esm27.collection.iot.devices.esm27.response.entity;

import java.util.List;

import cn.stronglink.esm27.collection.iot.devices.esm27.entity.SensorUseEntity;

/**
 * 设备设置实体
 * @author yuzhantao
 *
 */
public class DeviceOptionEntity extends DeviceEntity {
	/**
	 * 传感器的数量
	 */
	private int sensorCount;
	
	/**
	 * 设备使能标志(传感器、北斗、无线均使能)
	 */
	private List<SensorUseEntity> deviceUses;
	
	/**
	 * 无线通讯通道
	 */
	private int wirelessCommunicationChannel;
	
	/**
	 * 无线通讯速率
	 */
	private int wirelessCommunicationRate;
	
	/**
	 * 北斗刷新周期
	 */
	private short beidouRefreshCycle;
	
	/**
	 * 报警使能
	 */
	private List<AlarmOptionEntity> alarmOptions;
	
	/**
	 * 电池欠压阀值
	 */
	private float batteryUndervoltageThreshold;
	
	/**
	 * 电池欠压关机阀值
	 */
	private float batteryUndervoltageShutdownThreshold;
	
	/**
	 * 背光延迟时间
	 */
	private int backlightDelayTime;
	
	/**
	 * 背光模式
	 */
	private int backlightMode;
	
	/**
	 * 泵周期
	 */
	private int pumpCycle;
	
	/**
	 * 泵占空比
	 */
	private int pumpDutyRatio;
	
	/**
	 * 当前时间
	 */
	private long currentTime;

	public int getSensorCount() {
		return sensorCount;
	}

	public void setSensorCount(int sensorCount) {
		this.sensorCount = sensorCount;
	}

	public List<SensorUseEntity> getDeviceUses() {
		return deviceUses;
	}

	public void setDeviceUses(List<SensorUseEntity> deviceUses) {
		this.deviceUses = deviceUses;
	}

	public int getWirelessCommunicationChannel() {
		return wirelessCommunicationChannel;
	}

	public void setWirelessCommunicationChannel(int wirelessCommunicationChannel) {
		this.wirelessCommunicationChannel = wirelessCommunicationChannel;
	}

	public int getWirelessCommunicationRate() {
		return wirelessCommunicationRate;
	}

	public void setWirelessCommunicationRate(int wirelessCommunicationRate) {
		this.wirelessCommunicationRate = wirelessCommunicationRate;
	}

	public short getBeidouRefreshCycle() {
		return beidouRefreshCycle;
	}

	public void setBeidouRefreshCycle(short beidouRefreshCycle) {
		this.beidouRefreshCycle = beidouRefreshCycle;
	}

	public List<AlarmOptionEntity> getAlarmOptions() {
		return alarmOptions;
	}

	public void setAlarmOptions(List<AlarmOptionEntity> alarmOptions) {
		this.alarmOptions = alarmOptions;
	}

	public float getBatteryUndervoltageThreshold() {
		return batteryUndervoltageThreshold;
	}

	public void setBatteryUndervoltageThreshold(float batteryUndervoltageThreshold) {
		this.batteryUndervoltageThreshold = batteryUndervoltageThreshold;
	}

	public float getBatteryUndervoltageShutdownThreshold() {
		return batteryUndervoltageShutdownThreshold;
	}

	public void setBatteryUndervoltageShutdownThreshold(float batteryUndervoltageShutdownThreshold) {
		this.batteryUndervoltageShutdownThreshold = batteryUndervoltageShutdownThreshold;
	}

	public int getBacklightDelayTime() {
		return backlightDelayTime;
	}

	public void setBacklightDelayTime(int backlightDelayTime) {
		this.backlightDelayTime = backlightDelayTime;
	}

	public int getBacklightMode() {
		return backlightMode;
	}

	public void setBacklightMode(int backlightMode) {
		this.backlightMode = backlightMode;
	}

	public int getPumpCycle() {
		return pumpCycle;
	}

	public void setPumpCycle(int pumpCycle) {
		this.pumpCycle = pumpCycle;
	}

	public int getPumpDutyRatio() {
		return pumpDutyRatio;
	}

	public void setPumpDutyRatio(int pumpDutyRatio) {
		this.pumpDutyRatio = pumpDutyRatio;
	}

	public long getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(long currentTime) {
		this.currentTime = currentTime;
	}

	
}
