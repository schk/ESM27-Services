package cn.stronglink.esm27.collection.iot.devices.esm27.response.entity;

/**
 * ESM27设备上行数据返回的实体
 * @author yuzhantao
 *
 */
public class ESM27ResponseMessageEntity {
	/**
	 * 设备地址
	 */
	private byte deviceAddress;
	
	/**
	 * 设备状态
	 */
	private byte deviceStatus;
	
	/**
	 * 命令码
	 */
	private byte commandCode;
	
	/**
	 * 命令执行状态
	 */
	private byte commandRunStatus;
	
	/**
	 * 数据
	 */
	private byte[] datas;

	public byte getDeviceAddress() {
		return deviceAddress;
	}

	public void setDeviceAddress(byte deviceAddress) {
		this.deviceAddress = deviceAddress;
	}

	public byte getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(byte deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	public byte getCommandCode() {
		return commandCode;
	}

	public void setCommandCode(byte commandCode) {
		this.commandCode = commandCode;
	}

	public byte getCommandRunStatus() {
		return commandRunStatus;
	}

	public void setCommandRunStatus(byte commandRunStatus) {
		this.commandRunStatus = commandRunStatus;
	}

	public byte[] getDatas() {
		return datas;
	}

	public void setDatas(byte[] datas) {
		this.datas = datas;
	}
}
