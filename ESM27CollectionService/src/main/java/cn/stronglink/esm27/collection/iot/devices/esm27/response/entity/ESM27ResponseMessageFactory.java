package cn.stronglink.esm27.collection.iot.devices.esm27.response.entity;

/**
 * ESM27上行数据对象生成工厂
 * @author yuzhantao
 *
 */
public class ESM27ResponseMessageFactory {
	public Object CreateESM27Entity(byte deviceAddress,byte deviceStatus,
			byte commandCode,byte commandRunStatus,byte[] datas) {
		ESM27ResponseMessageEntity entity = new ESM27ResponseMessageEntity();
		entity.setDeviceAddress(deviceAddress);
		entity.setDeviceStatus(deviceStatus);
		entity.setCommandCode(commandCode);
		entity.setCommandRunStatus(commandRunStatus);
		entity.setDatas(datas);
		
		return entity;
	}
}
