package cn.stronglink.esm27.collection.iot.devices.esm27.response.entity;

/**
 * 传感器的基本属性
 * @author yuzhantao
 *
 */
public class SensorBasePropertyEntity extends DeviceEntity{
	/**
	 * 传感器的通道编号
	 */
	private byte sensorChannelNumber;
	/**
	 * 传感器编号
	 */
	private int sensorId;
	/**
	 * 传感器版本
	 */
	private byte sensorVersion;
	/**
	 * 气体编号
	 */
	private byte gasId;
	
	/**
	 * 气体测量单位的Id
	 */
	private byte gasUnitId;
	/**
	 * 小数位数
	 */
	private byte decimalDigits;
	/**
	 * 量程
	 */
	private short range;
	public byte getSensorChannelNumber() {
		return sensorChannelNumber;
	}
	public void setSensorChannelNumber(byte sensorChannelNumber) {
		this.sensorChannelNumber = sensorChannelNumber;
	}
	public int getSensorId() {
		return sensorId;
	}
	public void setSensorId(int sensorId) {
		this.sensorId = sensorId;
	}
	public byte getSensorVersion() {
		return sensorVersion;
	}
	public void setSensorVersion(byte sensorVersion) {
		this.sensorVersion = sensorVersion;
	}
	public byte getGasId() {
		return gasId;
	}
	public void setGasId(byte gasId) {
		this.gasId = gasId;
	}
	public byte getGasUnitId() {
		return gasUnitId;
	}
	public void setGasUnitId(byte gasUnitId) {
		this.gasUnitId = gasUnitId;
	}
	public byte getDecimalDigits() {
		return decimalDigits;
	}
	public void setDecimalDigits(byte decimalDigits) {
		this.decimalDigits = decimalDigits;
	}
	public short getRange() {
		return range;
	}
	public void setRange(short range) {
		this.range = range;
	}
	
	
}
