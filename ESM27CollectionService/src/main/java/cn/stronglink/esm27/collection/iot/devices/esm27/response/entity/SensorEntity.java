package cn.stronglink.esm27.collection.iot.devices.esm27.response.entity;

public class SensorEntity {
	/**
	 * 测量的值
	 */
	private float value;
	/**
	 * 气体名称的Id
	 */
	private int typeId;
	/**
	 * 气体名称
	 */
	private String name;
	/**
	 * 气体单位
	 */
	private String unit;
	/**
	 * 气体单位的Id
	 */
	private int unitId;
	/**
	 * 小数的位数
	 */
	private byte decimalDigits;
	
	public float getValue() {
		return value;
	}
	public void setValue(float value) {
		this.value = value;
	}
	
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public int getUnitId() {
		return unitId;
	}
	public void setUnitId(int unitId) {
		this.unitId = unitId;
	}
	public byte getDecimalDigits() {
		return decimalDigits;
	}
	public void setDecimalDigits(byte decimalDigits) {
		this.decimalDigits = decimalDigits;
	}
}
