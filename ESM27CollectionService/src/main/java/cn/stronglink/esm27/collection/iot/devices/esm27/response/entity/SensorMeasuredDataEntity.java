package cn.stronglink.esm27.collection.iot.devices.esm27.response.entity;

import java.util.List;

import cn.stronglink.esm27.collection.iot.devices.esm27.entity.AlarmInfoEntity;

/**
 * 传感器测量的数据
 * @author yuzhantao
 *
 */
public class SensorMeasuredDataEntity extends DeviceEntity {
	/**
	 * 传感器数值
	 */
	private float[] sensorValues;
	/**
	 * 经度
	 */
	private double longitude;
	/**
	 * 纬度
	 */
	private double latitude;
	/**
	 * 电池电压
	 */
	private float batteryVoltage;
	/**
	 * 报警信息
	 */
	private List<AlarmInfoEntity> alarms;
	/**
	 * 泵的状态
	 */
	private float pumpState;
	
	private String userId;
	
	private String roomId;
	
	public float[] getSensorValues() {
		return sensorValues;
	}
	public void setSensorValues(float[] sensorValues) {
		this.sensorValues = sensorValues;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public float getBatteryVoltage() {
		return batteryVoltage;
	}
	public void setBatteryVoltage(float batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}
	
	public List<AlarmInfoEntity> getAlarms() {
		return alarms;
	}
	public void setAlarms(List<AlarmInfoEntity> alarms) {
		this.alarms = alarms;
	}
	public float getPumpState() {
		return pumpState;
	}
	public void setPumpState(float pumpState) {
		this.pumpState = pumpState;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
}
