package cn.stronglink.esm27.collection.iot.devices.esm27.response.entity;

/**
 * 传感器设置的实例
 * @author yuzhantao
 *
 */
public class SensorOptionEntity extends DeviceEntity {
	/**
	 * 感器通道号=00传感器模块1（0～6表示传感器1～7）
	 */
	private byte sensorChannelNumber;

	/**
	 * 低报警点
	 */
	private float alAlarmThreshold;
	/**
	 * 高报警点
	 */
	private float ahAlarmThreshold;
	
	/**
	 * STEL报警点
	 */
	private float stelAlarmThreshold;
	
	/**
	 * TWA报警点
	 */
	private float twaAlarmThreshold;
	
	/**
	 * 增益
	 */
	private float gain;
	/**
	 * 零点AD
	 */
	private short ZeroPointAD;
	/**
	 * 系统时间
	 */
	private long systemTime;
	public byte getSensorChannelNumber() {
		return sensorChannelNumber;
	}
	public void setSensorChannelNumber(byte sensorChannelNumber) {
		this.sensorChannelNumber = sensorChannelNumber;
	}
	public float getAlAlarmThreshold() {
		return alAlarmThreshold;
	}
	public void setAlAlarmThreshold(float alAlarmThreshold) {
		this.alAlarmThreshold = alAlarmThreshold;
	}
	public float getAhAlarmThreshold() {
		return ahAlarmThreshold;
	}
	public void setAhAlarmThreshold(float ahAlarmThreshold) {
		this.ahAlarmThreshold = ahAlarmThreshold;
	}
	public float getStelAlarmThreshold() {
		return stelAlarmThreshold;
	}
	public void setStelAlarmThreshold(float stelAlarmThreshold) {
		this.stelAlarmThreshold = stelAlarmThreshold;
	}
	public float getTwaAlarmThreshold() {
		return twaAlarmThreshold;
	}
	public void setTwaAlarmThreshold(float twaAlarmThreshold) {
		this.twaAlarmThreshold = twaAlarmThreshold;
	}
	public float getGain() {
		return gain;
	}
	public void setGain(float gain) {
		this.gain = gain;
	}
	public short getZeroPointAD() {
		return ZeroPointAD;
	}
	public void setZeroPointAD(short zeroPointAD) {
		ZeroPointAD = zeroPointAD;
	}
	public long getSystemTime() {
		return systemTime;
	}
	public void setSystemTime(long systemTime) {
		this.systemTime = systemTime;
	}
	
}
