package cn.stronglink.esm27.collection.iot.devices.esm27.response.entity;

import java.util.List;

import cn.stronglink.esm27.collection.iot.devices.esm27.entity.AlarmInfoEntity;

/**
 * 传感器实时数据
 * @author yuzhantao
 *
 */
public class SensorRealtimeDataEntity extends DeviceEntity {
	/**
	 * 传感器数值
	 */
	private SensorEntity[] sensors;
	/**
	 * 经度
	 */
	private double longitude;
	/**
	 * 纬度
	 */
	private double latitude;
	/**
	 * 电池电压
	 */
	private float batteryVoltage;
	/**
	 * 传感器3的TWA报警点
	 */
	private List<AlarmInfoEntity> alarms;;
	/**
	 * 泵的状态
	 */
	private byte pumpState;
	
	public SensorEntity[] getSensors() {
		return sensors;
	}
	public void setSensors(SensorEntity[] sensors) {
		this.sensors = sensors;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public float getBatteryVoltage() {
		return batteryVoltage;
	}
	public void setBatteryVoltage(float batteryVoltage) {
		this.batteryVoltage = batteryVoltage;
	}
	
	public List<AlarmInfoEntity> getAlarms() {
		return alarms;
	}
	public void setAlarms(List<AlarmInfoEntity> alarms) {
		this.alarms = alarms;
	}
	public byte getPumpState() {
		return pumpState;
	}
	public void setPumpState(byte pumpState) {
		this.pumpState = pumpState;
	}
}
