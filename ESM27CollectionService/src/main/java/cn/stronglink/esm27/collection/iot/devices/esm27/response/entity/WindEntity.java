package cn.stronglink.esm27.collection.iot.devices.esm27.response.entity;

public class WindEntity extends DeviceEntity  {
	/**
	 * 风速
	 */
	float windSpeed;
	/**
	 * 风向
	 */
	float winDirection;
	/**
	 * 温度
	 */
	float temperature;
	/**
	 * 湿度
	 */
	float humidity;
	/**
	 * 气压
	 */
	float pressure;
	/**
	 * PM2.5
	 */
	float pm2_5;
	/**
	 * 噪音
	 */
	float noise;
	
	String userId;
	
	String roomId;
	
	public float getWindSpeed() {
		return windSpeed;
	}
	public void setWindSpeed(float windSpeed) {
		this.windSpeed = windSpeed;
	}
	public float getWinDirection() {
		return winDirection;
	}
	public void setWinDirection(float winDirection) {
		this.winDirection = winDirection;
	}
	public float getTemperature() {
		return temperature;
	}
	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}
	public float getHumidity() {
		return humidity;
	}
	public void setHumidity(float humidity) {
		this.humidity = humidity;
	}
	public float getPressure() {
		return pressure;
	}
	public void setPressure(float pressure) {
		this.pressure = pressure;
	}
	public float getPm2_5() {
		return pm2_5;
	}
	public void setPm2_5(float pm2_5) {
		this.pm2_5 = pm2_5;
	}
	public float getNoise() {
		return noise;
	}
	public void setNoise(float noise) {
		this.noise = noise;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	
	
}
