package cn.stronglink.esm27.collection.iot.devices.esm27.response.handle;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;

import cn.stronglink.esm27.collection.core.message.IMessageHandle;
import cn.stronglink.esm27.collection.core.util.ByteUtil;
import cn.stronglink.esm27.collection.core.util.ContextUtils;
import cn.stronglink.esm27.collection.iot.devices.esm27.ESM27Server;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.ESM27MessageTools;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.MQMessageOfESM27;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.ESM27RequestMessageFactory;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.OpenSerialPortEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.PushSensorDataEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.entity.AutoAlarmEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.entity.ESM27ResponseMessageEntity;
import cn.stronglink.esm27.collection.mq.TopicSender;
import io.netty.channel.ChannelHandlerContext;

/**
 * 主动上传报警的处理
 * 
 * @author yuzhantao
 *
 */
public class AutoAlarmHandle implements IMessageHandle<ESM27ResponseMessageEntity, Object> {
	private final static String ACTION_CODE = "AutoAlarm";
	private final static Logger logger = LogManager.getLogger(AutoAlarmHandle.class.getName());
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	private ESM27Server esm27Server = (ESM27Server) ContextUtils.getBean("esm27Server"); // esm27服务器

	@Override
	public boolean isHandle(ESM27ResponseMessageEntity t) {
		if (t.getCommandCode() == 0x0B) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, ESM27ResponseMessageEntity t) {
		// 由于报警中没有带出传感器的数据，所以向硬件发送获取全部传感器信息的命令
		try {
			this.getAllSensorDatas(t.getDeviceAddress());
		}catch(Exception e) {
			logger.error(e);
		}
		
		try {
			AutoAlarmEntity autoAlarm = new AutoAlarmEntity();
			autoAlarm.setDeviceAddress(t.getDeviceAddress());
			// 获取报警信息
			int alarmData = ByteUtil.byteArrayToInt(t.getDatas());
			autoAlarm.setAlarmInfoList(ESM27MessageTools.getAlarmInfoList(alarmData, false));

			// 获取打开串口的实例对象
			OpenSerialPortEntity sp = PushSensorDataEntity.getInstance().getOpenSerialPortEntity();
			if(sp!=null) {
				autoAlarm.setUserId(sp.getUserId());
				autoAlarm.setRoomId(sp.getRoomId());
			}
			
			MQMessageOfESM27 message = new MQMessageOfESM27();
			message.setActioncode(ACTION_CODE);
			message.setTimeStamp(System.currentTimeMillis());
			message.setAwsPostdata(autoAlarm);
			message.setIsSuccess(true);

			String json = JSON.toJSONString(message);
			topicSender.send(json);
			logger.info("发送[{}]到MQ完成:{}", "主动上传报警", json);
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	/**
	 * 获取所有传感器信息
	 * @param deviceAddress
	 * @throws IOException
	 */
	private void getAllSensorDatas(byte deviceAddress) throws IOException {
		byte[] sendDatas = ESM27RequestMessageFactory.createMessage(deviceAddress, (byte) 0x0A, null);
		esm27Server.send(sendDatas);
		logger.info("发送消息到ESM27:{}", ByteUtil.byteArrToHexString(sendDatas, true));
	}
}
