package cn.stronglink.esm27.collection.iot.devices.esm27.response.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;

import cn.stronglink.esm27.collection.core.message.IMessageHandle;
import cn.stronglink.esm27.collection.core.util.ContextUtils;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.MQMessageOfESM27;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.entity.DeviceEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.entity.ESM27ResponseMessageEntity;
import cn.stronglink.esm27.collection.mq.TopicSender;
import io.netty.channel.ChannelHandlerContext;

/**
 * 呼叫设备的处理
 * @author yuzhantao
 *
 */
public class CallReturnHandle implements IMessageHandle<ESM27ResponseMessageEntity, Object> {
	private final static String ACTION_CODE="CallReturn";
	private final static Logger logger = LogManager.getLogger(CallReturnHandle.class.getName());
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	
	@Override
	public boolean isHandle(ESM27ResponseMessageEntity t) {
		if(t.getCommandCode()==0x00) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, ESM27ResponseMessageEntity t) {
		DeviceEntity device = new DeviceEntity();
		device.setDeviceAddress(t.getDeviceAddress());
		
		MQMessageOfESM27 message = new MQMessageOfESM27();
		message.setActioncode(ACTION_CODE);
		message.setTimeStamp(System.currentTimeMillis());
		message.setAwsPostdata(device);
		message.setIsSuccess(true);
		
		String json = JSON.toJSONString(message);
		topicSender.send(json);
		logger.info("发送[{}]到MQ完成:{}","空操作信息的反馈信息",json);
		return null;
	}

}
