package cn.stronglink.esm27.collection.iot.devices.esm27.response.handle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;

import cn.stronglink.esm27.collection.core.message.IMessageHandle;
import cn.stronglink.esm27.collection.core.util.ByteUtil;
import cn.stronglink.esm27.collection.core.util.ContextUtils;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.ESM27MessageTools;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.GasDataManager;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.MQMessageOfESM27;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.entity.ESM27ResponseMessageEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.entity.SensorEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.entity.SensorRealtimeDataEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.util.GISUtil;
import cn.stronglink.esm27.collection.mq.TopicSender;
import io.netty.channel.ChannelHandlerContext;

/**
 * 获取设备实时数据
 * @author yuzhantao
 *
 */
public class GetDeviceRealtimeDataReturnHandle implements IMessageHandle<ESM27ResponseMessageEntity, Object> {
	private static Logger logger = LogManager.getLogger(GetDeviceRealtimeDataReturnHandle.class.getName());
	private final static int SENSOR_DATA_SIZE=7;	// 传感器数据的尺寸
	private final static String ACTION_CODE="GetDeviceRealtimeDataReturn";
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	
	@Override
	public boolean isHandle(ESM27ResponseMessageEntity t) {
		if(t.getCommandCode()==0x05 && t.getDatas().length!=28) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, ESM27ResponseMessageEntity t) {
		int offset=0;
		SensorRealtimeDataEntity entity = new SensorRealtimeDataEntity();
		entity.setDeviceAddress(t.getDeviceAddress());
		
		final int SENSOR_COUNT = (t.getDatas().length-17)/SENSOR_DATA_SIZE; // 获取传感器数量
		// 获取传感器的值
		SensorEntity[] sensors=new SensorEntity[SENSOR_COUNT];
		for(int i=0;i<SENSOR_COUNT;i++) {
			offset=i*SENSOR_DATA_SIZE;
			SensorEntity sensor = new SensorEntity();
			sensor.setValue(ByteUtil.bytesToFloat(t.getDatas(), offset));
			sensor.setTypeId(t.getDatas()[offset+4]);
			sensor.setName(GasDataManager.findGasName(sensor.getTypeId()));
			sensor.setUnitId(t.getDatas()[offset+5]);
			sensor.setUnit(GasDataManager.findGasUnit(sensor.getUnitId()));
			sensor.setDecimalDigits(t.getDatas()[offset+5]);
			sensors[i]=sensor;
		}
		entity.setSensors(sensors);
		
		// 获取经度
		offset+=4;
		entity.setLongitude(GISUtil.bd2Gps(ByteUtil.bytesToFloat(t.getDatas(),offset)));
		// 获取纬度
		offset+=4;
		entity.setLatitude(GISUtil.bd2Gps(ByteUtil.bytesToFloat(t.getDatas(),offset)));
		// 获取电池电压
		offset+=4;
		entity.setBatteryVoltage(ByteUtil.bytesToFloat(t.getDatas(),offset));
		// 传感器的报警信息
		offset+=4;
		int alarmData = ByteUtil.byteArrayToInt(t.getDatas(),offset);
		entity.setAlarms(ESM27MessageTools.getAlarmInfoList(alarmData, false));
		// 泵状态
		offset+=1;
		entity.setPumpState(t.getDatas()[offset]);
		
		MQMessageOfESM27 message = new MQMessageOfESM27();
		message.setActioncode(ACTION_CODE);
		message.setTimeStamp(System.currentTimeMillis());
		message.setAwsPostdata(entity);
		message.setIsSuccess(true);
		
		String json = JSON.toJSONString(message);
		topicSender.send(json);
		logger.info("发送获取设备实时数据到MQ完成:{}",json);
		return null;
	}

}