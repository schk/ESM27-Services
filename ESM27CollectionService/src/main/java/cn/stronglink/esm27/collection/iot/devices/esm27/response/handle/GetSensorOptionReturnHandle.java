package cn.stronglink.esm27.collection.iot.devices.esm27.response.handle;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.alibaba.fastjson.JSON;

import cn.stronglink.esm27.collection.core.message.IMessageHandle;
import cn.stronglink.esm27.collection.core.util.ByteUtil;
import cn.stronglink.esm27.collection.core.util.ContextUtils;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.MQMessageOfESM27;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.entity.ESM27ResponseMessageEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.entity.SensorOptionEntity;
import cn.stronglink.esm27.collection.mq.TopicSender;
import io.netty.channel.ChannelHandlerContext;

/**
 * 获取传感器配置参数的反馈
 * @author yuzhantao
 *
 */
public class GetSensorOptionReturnHandle implements IMessageHandle<ESM27ResponseMessageEntity, Object> {
	private final static String ACTION_CODE="GetSensorOptionReturn";
	private final static Logger logger = LogManager.getLogger(GetSensorOptionReturnHandle.class.getName());
	private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");
	
	@Override
	public boolean isHandle(ESM27ResponseMessageEntity t) {
		if(t.getCommandCode()==0x04) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public Object handle(ChannelHandlerContext ctx, ESM27ResponseMessageEntity t) {
		SensorOptionEntity option = new SensorOptionEntity();
		option.setDeviceAddress(t.getDeviceAddress());
		// 设置传感器通道号=00传感器模块1（0～6表示传感器1～7）
		option.setSensorChannelNumber(Integer.valueOf(t.getDatas()[0]-1).byteValue());
		// 设置低报警点
		option.setAlAlarmThreshold(ByteUtil.bytesToFloat(t.getDatas(), 1));
		// 设置高报警点
		option.setAhAlarmThreshold(ByteUtil.bytesToFloat(t.getDatas(), 5));
		// 设置STEL报警点
		option.setStelAlarmThreshold(ByteUtil.bytesToFloat(t.getDatas(), 9));
		// 设置TWA报警点
		option.setTwaAlarmThreshold(ByteUtil.bytesToFloat(t.getDatas(), 13));
		// 设置增益
		option.setGain(ByteUtil.bytesToFloat(t.getDatas(), 17));
		// 设置零点AD
		option.setZeroPointAD(ByteUtil.byteArrToShort(t.getDatas(), 21));
		// 设置标定时间
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(Calendar.YEAR,2000+t.getDatas()[23]);	//设置年
		gc.set(Calendar.MONTH, t.getDatas()[24]);		//这里0是1月..以此向后推
		gc.set(Calendar.DAY_OF_MONTH, t.getDatas()[25]);//设置天
		gc.set(Calendar.HOUR_OF_DAY, t.getDatas()[26]);	//设置小时
		gc.set(Calendar.MINUTE, t.getDatas()[27]);		//设置分
		gc.set(Calendar.SECOND, t.getDatas()[28]);		//设置秒
		option.setSystemTime(gc.getTimeInMillis());
		
		MQMessageOfESM27 message = new MQMessageOfESM27();
		message.setActioncode(ACTION_CODE);
		message.setTimeStamp(System.currentTimeMillis());
		message.setAwsPostdata(option);
		message.setIsSuccess(true);
		switch(t.getCommandRunStatus()) {
		case 0:
			message.setIsSuccess(true);
			break;
		case 2:
			message.setIsSuccess(false);
			message.setErrorMessage("通道号无效");
			break;
		case 3:
			message.setIsSuccess(false);
			message.setErrorMessage("通道未使能");
			break;
		case 4:
			message.setIsSuccess(false);
			message.setErrorMessage("通道已失效");
			break;
		default:
			message.setIsSuccess(false);
			message.setErrorMessage("未知信息");
			break;
		}
		
		String json = JSON.toJSONString(message);
		topicSender.send(json);
		logger.info("发送[{}]到MQ完成:{}","获取传感器设置参数的反馈信息",json);
		
		return null;
	}

}

