package cn.stronglink.esm27.collection.iot.devices.esm27.response.handle;

import cn.stronglink.esm27.collection.core.message.IMessageHandle;
import cn.stronglink.esm27.collection.core.util.ByteUtil;
import cn.stronglink.esm27.collection.core.util.ContextUtils;
import cn.stronglink.esm27.collection.iot.devices.esm27.message.MQMessageOfESM27;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.OpenSerialPortEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.PushSensorDataEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.entity.ESM27ResponseMessageEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.entity.WindEntity;
import cn.stronglink.esm27.collection.mq.TopicSender;
import com.alibaba.fastjson.JSON;
import io.netty.channel.ChannelHandlerContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GetWindRealtimeDatasReturnHandle2 implements IMessageHandle<ESM27ResponseMessageEntity, Object> {
    private static Logger logger = LogManager.getLogger(GetDeviceRealtimeDataReturnHandle.class.getName());
    private final static String ACTION_CODE = "GetWindRealtimeDatasReturn";
    private TopicSender topicSender = (TopicSender) ContextUtils.getBean("topicSender");

    @Override
    public boolean isHandle(ESM27ResponseMessageEntity t) {
        if (t.getCommandCode() == 0x05 && t.getDatas().length == 28) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Object handle(ChannelHandlerContext ctx, ESM27ResponseMessageEntity t) {
        WindEntity we = new WindEntity();
        // 将t.getDatas()转为16进制字符串
        logger.info("接收到的实时数据:{}", ByteUtil.byteArrToHexString(t.getDatas()));
        we.setWindSpeed(ByteUtil.convertByteArrayToFloat(t.getDatas(), 0));
        we.setWinDirection(ByteUtil.convertByteArrayToFloat(t.getDatas(), 4));
        we.setTemperature(ByteUtil.convertByteArrayToFloat(t.getDatas(), 8));
        we.setHumidity(ByteUtil.convertByteArrayToFloat(t.getDatas(), 12));
        we.setPressure(ByteUtil.bytesToFloat(t.getDatas(), 16));
        we.setPm2_5(ByteUtil.bytesToFloat(t.getDatas(), 20));
        we.setNoise(ByteUtil.bytesToFloat(t.getDatas(), 24));
        we.setDeviceAddress(t.getDeviceAddress());

        // 获取打开串口的实例对象
        OpenSerialPortEntity sp = PushSensorDataEntity.getInstance().getOpenSerialPortEntity();
        if (sp != null) {
            we.setUserId(sp.getUserId());
            we.setRoomId(sp.getRoomId());
        }

        MQMessageOfESM27 message = new MQMessageOfESM27();
        message.setActioncode(ACTION_CODE);
        message.setTimeStamp(System.currentTimeMillis());
        message.setAwsPostdata(we);
        message.setIsSuccess(true);

        String json = JSON.toJSONString(message);
        topicSender.send(json);
        logger.info("发送获取风力实时数据到MQ完成:{}", json);
        return null;
    }

    public static void main(String[] args) {
        // 输入的字节数组
        // 字节数组转16进制字符串
//        byte[] byteArray = {0x36, 0x3B, 0x3F, (byte) 0x83, 0x00, 0x00, 0x01, 0x4E};
//        byte[] byteArray = {0x36, 0x3B, 0x3F, (byte) 0x83, 0x00, 0x00, 0x01, 0x4E};
        byte[] byteArray = {(byte) 0x10, (byte) 0x65, 0x40,  0x56, (byte) 0xCE, (byte) 0x96, 0x43, (byte) 0x17, (byte) 0xEC, (byte) 0x7C, 0x41, (byte) 0xC4, (byte) 0x91,0x79,0x42, (byte) 0x88};
        float floatValue = ByteUtil.convertByteArrayToFloat(byteArray, 0);
        float floatValue2 = ByteUtil.convertByteArrayToFloat(byteArray, 4);
        float floatValue3 = ByteUtil.convertByteArrayToFloat(byteArray, 8);
        float floatValue4 = ByteUtil.convertByteArrayToFloat(byteArray, 12);
//        int intValue2 = ByteUtil.convertByteArrayToInteger(byteArray, 6, 2);
        System.out.println("floatValue: " + floatValue);
        System.out.println("floatValue2: " + floatValue2);
        System.out.println("floatValue3: " + floatValue3);
        System.out.println("floatValue3: " + floatValue4);
//        System.out.println("intValue2: " + intValue2);
    }


}
