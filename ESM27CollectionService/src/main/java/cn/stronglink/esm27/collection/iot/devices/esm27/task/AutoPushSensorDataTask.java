package cn.stronglink.esm27.collection.iot.devices.esm27.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.config.TriggerTask;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import cn.stronglink.esm27.collection.iot.devices.esm27.ESM27Server;
import cn.stronglink.esm27.collection.iot.devices.esm27.request.entity.ESM27RequestMessageFactory;

/**
 * 自动推送传感器数据的任务
 * 
 * @author yuzhantao
 *
 */
//@Component("autoPushSensorDataTask")
//@Lazy(false)
//@EnableScheduling
public class AutoPushSensorDataTask implements SchedulingConfigurer {
	private static Logger logger = LogManager.getLogger(AutoPushSensorDataTask.class.getName());
	private static final int SEND_DATA_SPAN_MILLISECOND=3500;
	private static final int DEVICE_COUNT = 16; // 设备数量
	private static final String DEFAULT_CRON = "0/61 * * * * ? ";
	private String cron = DEFAULT_CRON;
	private List<Integer> deviceIds;
	
	@Resource
	private ESM27Server esm27Server; // esm27服务器

	private boolean isRun = false;

	private int deviceCount = DEVICE_COUNT;
	
	private TriggerTask triggerTask=null;
//	private Timer timer;

//	ExecutorService timer = Executors.newSingleThreadExecutor();
	ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();
	
	public AutoPushSensorDataTask() {
		deviceIds = new ArrayList<>();
		for(int i = 0;i<8;i++) {
			deviceIds.add(i);
		}
	}

	public List<Integer> getDeviceIds() {
		return deviceIds;
	}

	public void setDeviceIds(List<Integer> deviceIds) {
		this.deviceIds = deviceIds;
	}

	public int getDeviceCount() {
		return deviceCount;
	}

	public void setDeviceCount(int deviceCount) {
		this.deviceCount = deviceCount;
	}

	/**
	 * 获取任务执行规则
	 * 
	 * @return
	 */
	public String getCron() {
		return cron;
	}

	/**
	 * 设置任务执行规则
	 * 
	 * @param cron
	 */
	public void setCron(String cron) {
		if (cron == null || cron.isEmpty()) {
			isRun = false;
		} else {
//			this.cron = cron;
			isRun = true;
		}
	}

	/**
	 * 定时获取传感器实时数据的任务
	 * 
	 * @throws IOException
	 */
//	@Scheduled(cron = "0/5 * * * * ?")
	protected void getSensorRealtimeDataTask(byte deviceAddress) throws IOException {
		if (esm27Server.isConnection()) {
			byte[] datas = ESM27RequestMessageFactory.createMessage(deviceAddress, (byte) 0x0A, null);
			esm27Server.send(datas);
			logger.info("定时获取传感器实时数据命令已发送");
		}
	}

	private String taskCode = null;
	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
		taskCode=UUID.randomUUID().toString();
		if(this.triggerTask!=null) return;
		this.triggerTask = new TriggerTask(new Runnable() {
			String code = taskCode;
			@Override
			public void run() {
				logger.info("=======");
//				if (isRun == false || this.code.equals(taskCode)==false)
//					return;
//				logger.info("=======================================");
//				int i = 0;
//				// 发送获取气体请求到i个设备
//				
//				for (i = 0; i < deviceIds.size(); i++) {
//					Integer id = deviceIds.get(i);
//					if(isRun) {
//						timer.schedule(new PushGasTimer(esm27Server, id.byteValue()), 3*(id+1), TimeUnit.SECONDS);
//					}
//				}
//				// 发送风力请求
//				if(isRun) {
//					timer.schedule(new PushWindTimer(esm27Server), 3*(i+1), TimeUnit.SECONDS);
//				}
			}
		}, new Trigger() {
			@Override
			public Date nextExecutionTime(TriggerContext triggerContext) {
				// 任务触发，可修改任务的执行周期
				CronTrigger trigger = new CronTrigger(cron);
				Date nextExec = trigger.nextExecutionTime(triggerContext);
				return nextExec;
			}
		});
		
		taskRegistrar.addTriggerTask(triggerTask);
		
	}
}
