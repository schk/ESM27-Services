package cn.stronglink.esm27.collection.iot.devices.esm27.util;

/**
 * GIS坐标转换工具
 * @author yuzhantao
 *
 */
public class GISUtil {
	/**
	 * 北斗转GPS
	 * @param value
	 * @return
	 */
	public static double bd2Gps(double value) {
		return Math.floor(value)+(value - Math.floor(value)) * (double)100.0 / (double)60.0;
	}
}
