package cn.stronglink.esm27.collection.mq;

import javax.jms.Topic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

@Component("topicSender")
public class TopicSender {
	@Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
	@Autowired
    private Topic topic;
	
	/**
	 * 发送MQ到指定主题
	 * @param topicName
	 * @param message
	 */
	public void send(final String message){
		jmsMessagingTemplate.convertAndSend(topic,message);
	}

}
