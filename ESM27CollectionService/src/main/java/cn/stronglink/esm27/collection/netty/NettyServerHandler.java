package cn.stronglink.esm27.collection.netty;

import cn.stronglink.esm27.collection.core.message.MessageHandleContext;
import cn.stronglink.esm27.collection.core.util.ByteUtil;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.coder.ESM27Coder;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.entity.ESM27ResponseMessageEntity;
import cn.stronglink.esm27.collection.iot.devices.esm27.response.handle.*;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.ReferenceCountUtil;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentHashMap;

public class NettyServerHandler extends ChannelInboundHandlerAdapter {

    private final Logger log = LogManager.getLogger(getClass());
    private final MessageHandleContext<ESM27ResponseMessageEntity, Object> handleContext = new MessageHandleContext<>();
    private ESM27Coder coder = new ESM27Coder();
    public static final ChannelGroup channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    public NettyServerHandler(int protocolVersion) {
        if (protocolVersion == 1) {
            handleContext.addHandleClass(new GetWindRealtimeDatasReturnHandle());
        } else {
            handleContext.addHandleClass(new GetWindRealtimeDatasReturnHandle2());
        }
        handleContext.addHandleClass(new AutoAlarmHandle());        // 自动上传报警的处理
        handleContext.addHandleClass(new CallReturnHandle());        // 呼叫设备的处理
        handleContext.addHandleClass(new GetAllSensorDataReturnHandle());            // 获取所有传感器数据
        handleContext.addHandleClass(new GetDeviceBasePropertyReturnHandle());        // 获取设备基本属性的反馈处理
        handleContext.addHandleClass(new GetDeviceOptionReturnHandle());            // 获取设备设置信息的反馈处理
        handleContext.addHandleClass(new GetDeviceRealtimeDataReturnHandle());        // 获取设备实时数据
        handleContext.addHandleClass(new GetSensorBasePropertyReturnHandle());        // 获取传感器基本属性的反馈处理
        handleContext.addHandleClass(new GetSensorOptionReturnHandle());            // 获取传感器设置信息的反馈处理
        handleContext.addHandleClass(new SetDateTimeReturnHandle());                // 设置时间的反馈处理
        handleContext.addHandleClass(new SetDeviceOptionReturnHandle());            // 设置设备选项的反馈处理
        handleContext.addHandleClass(new SetGasOptionReturnHandle());                // 设置传感器配置的反馈处理
        handleContext.addHandleClass(new SetSensorOptionReturnHandle());            // 设置传感器配置的反馈处理
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        try {
            InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
            String clientIp = insocket.getAddress().getHostAddress();
            ByteBuf in = (ByteBuf) msg;
            // 读取 ByteBuf 中的数据到 ByteBuffer
            ByteBuffer byteBuffer = ByteBuffer.allocate(in.readableBytes());
            in.readBytes(byteBuffer);
            // 接受到的消息打印
            log.info("IP:[{}]接收到消息:{} ", clientIp, ByteUtil.byteArrToHexString(byteBuffer.array()));
            Object obj = coder.encoder(byteBuffer);
            if (obj instanceof ESM27ResponseMessageEntity) {
                handleContext.handle(null, (ESM27ResponseMessageEntity) obj);
            }
            //响应客户端
//            this.channelWrite(ctx.channel().id(), msg);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 抛弃收到的数据
            ReferenceCountUtil.release(msg);
        }
    }


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
        String clientIp = insocket.getAddress().getHostAddress();
        int clientPort = insocket.getPort();
        //获取连接通道唯一标识
        ChannelId channelId = ctx.channel().id();
        //如果map中不包含此连接，就保存连接
        if (channelGroup.contains(ctx.channel())) {
            log.info("客户端【" + channelId + "】是连接状态，连接通道数量: " + channelGroup.size());
        } else {
            //保存连接
            channelGroup.add(ctx.channel());
            log.info("客户端【" + channelId + "】连接netty服务器[IP:" + clientIp + "--->PORT:" + clientPort + "]");
            log.info("连接通道数量: " + channelGroup.size());
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
        String clientIp = insocket.getAddress().getHostAddress();
        ChannelId channelId = ctx.channel().id();
        channelGroup.remove(ctx.channel());
        log.info("客户端【" + channelId + "】断开netty服务器[IP:" + clientIp + "]");
        log.info("连接通道数量: " + channelGroup.size());
    }


    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        InetSocketAddress insocket = (InetSocketAddress) ctx.channel().remoteAddress();
        String clientIp = insocket.getAddress().getHostAddress();
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent e = (IdleStateEvent) evt;
            if (e.state() == IdleState.READER_IDLE) {
                log.info("客户端【" + ctx.channel().id() + "】读超时，断开连接");
                ctx.close();
                log.info("客户端【" + ctx.channel().id() + "】断开netty服务器[IP:" + clientIp + "]");
                log.info("连接通道数量: " + channelGroup.size());
            } else if (e.state() == IdleState.WRITER_IDLE) {
                log.info("客户端【" + ctx.channel().id() + "】写超时，断开连接");
                ctx.close();
                log.info("客户端【" + ctx.channel().id() + "】断开netty服务器[IP:" + clientIp + "]");
                log.info("连接通道数量: " + channelGroup.size());
            } else if (e.state() == IdleState.ALL_IDLE) {
                log.info("客户端【" + ctx.channel().id() + "】读写超时，断开连接");
                ctx.close();
                log.info("客户端【" + ctx.channel().id() + "】断开netty服务器[IP:" + clientIp + "]");
                log.info("连接通道数量: " + channelGroup.size());
            }
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
        log.info(ctx.channel().id() + " 发生了错误,此连接被关闭" + "此时连通数量: " + channelGroup.size());
    }

}
