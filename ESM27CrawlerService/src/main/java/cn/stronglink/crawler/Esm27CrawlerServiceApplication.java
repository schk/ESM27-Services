package cn.stronglink.crawler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = { "cn.stronglink.crawler.service.handle.nrcc.vo", "cn.stronglink.crawler.vo" })
@EnableJpaRepositories(basePackages = { "cn.stronglink.crawler.service.handle.nrcc.vo", "cn.stronglink.crawler.vo" })
public class Esm27CrawlerServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(Esm27CrawlerServiceApplication.class, args);
	}
}
