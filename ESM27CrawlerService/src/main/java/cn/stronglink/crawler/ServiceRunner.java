package cn.stronglink.crawler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.Service.Listener;
import com.google.common.util.concurrent.Service.State;

import cn.stronglink.crawler.service.CrawlerService;

/**
 * 所有服务的入口类
 * @author yuzhantao
 *
 */
@Component
public class ServiceRunner implements ApplicationRunner {
	@Autowired
	CrawlerService crawlerService;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		crawlerService.setSite("http://service.nrcc.com.cn/Mobile/MyQuickWatch");

		crawlerService.addListener(new Listener() {
            @Override
            public void starting() {
                System.out.println("服务开始启动.....");
            }

            @Override
            public void running() {
                System.out.println("服务开始运行");;
            }

            @Override
            public void stopping(State from) {
                System.out.println("服务关闭中");
            }

            @Override
            public void terminated(State from) {
                System.out.println("服务终止");
            }

            @Override
            public void failed(State from, Throwable failure) {
                System.out.println("失败，cause：" + failure.getCause());
            }
        }, MoreExecutors.directExecutor());

//		crawlerService.startAsync().awaitRunning();
	}

}
