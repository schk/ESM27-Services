package cn.stronglink.crawler.common;

import java.lang.reflect.Field;

public class HtmlDataObject {
	public void setValue(String key, Object value) throws IllegalArgumentException, IllegalAccessException {
		Field[] fields = this.getClass().getDeclaredFields();
		for (Field field : fields) {
			HtmlKey hk = field.getDeclaredAnnotation(HtmlKey.class);
			if (hk == null)
				continue;
			if (hk.value() == null || hk.value().isEmpty() || !hk.value().equals(key))
				continue;
			field.setAccessible(true);
			field.set(this, value);
			break;
		}
	}
}
