package cn.stronglink.crawler.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.stronglink.crawler.service.handle.nrcc.vo.ChemicalInfoVoRepository;
import cn.stronglink.crawler.vo.ChemicalVo;
import cn.stronglink.crawler.vo.ChemicalVoRepository;

/**
 * 危化品服务
 * 
 * @author yuzhantao
 *
 */
@Service
@Transactional
public class ChemicalService implements IChemicalService {
	@Autowired
	private ChemicalVoRepository chemicalVoRepository;
	@Autowired
	private ChemicalInfoVoRepository chemicalInfoVoRepository;
	@Override
	public Page<ChemicalVo> search(String key, int page, int size) {
		Pageable pageable = PageRequest.of(page, size,
				JpaSort.unsafe(Sort.Direction.ASC, "ABS(LENGTH(query_key)-LENGTH('"+key+"'))")); // 分页信息
		return chemicalVoRepository.search(key, pageable);
	}
//
//	private class ChemicalSpec implements Specification<ChemicalVo>{
//		private static final long serialVersionUID = 1L;
//
//		@Override
//		public Predicate toPredicate(Root<ChemicalVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
//			Join<ChemicalVo,ChemicalInfoVo> join = root.join("imStudent", JoinType.INNER);
//            Path<String> exp3 = join.get("name"); 
//            return criteriaBuilder.like(exp3, "%jy%");
//		}
//	}

	@Override
	public Object find(int id) {
		return chemicalInfoVoRepository.findById(id).get();
	}
}
