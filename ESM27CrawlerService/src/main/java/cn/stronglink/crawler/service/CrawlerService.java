package cn.stronglink.crawler.service;

import java.util.Iterator;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Sets;
import com.google.common.util.concurrent.AbstractExecutionThreadService;

/**
 * 爬虫服务
 * @author yuzhantao
 *
 */
@Service("crawlerService")
public class CrawlerService extends AbstractExecutionThreadService {
	private String site;
	private static final Set<ICrawlerHandle> handleSet = Sets.newConcurrentHashSet();
	
	@Autowired
	ICrawlerHandle nrccHandle;
	
	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	@Override
	protected void run() throws Exception {
		if (handleSet.size() == 0) {
			handleSet.add(nrccHandle);
		}
		Iterator<ICrawlerHandle> iterator = handleSet.iterator();
		while(iterator.hasNext()) {
			ICrawlerHandle handle = iterator.next();
			if(handle.isHandle(this.site)) {
				handle.handle(this.site);
				break;
			}
		}
	}

}
