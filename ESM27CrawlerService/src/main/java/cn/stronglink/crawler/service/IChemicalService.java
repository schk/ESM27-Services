package cn.stronglink.crawler.service;

import org.springframework.data.domain.Page;

import cn.stronglink.crawler.vo.ChemicalVo;

public interface IChemicalService {
	Page<ChemicalVo> search(String key,int page,int size);
	
	Object find(int id);
}
