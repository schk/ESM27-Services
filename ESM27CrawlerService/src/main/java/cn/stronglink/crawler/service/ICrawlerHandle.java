package cn.stronglink.crawler.service;

public interface ICrawlerHandle {
	/**
	 * 通过网址判断是否处理
	 * @param site
	 * @return
	 */
	boolean isHandle(String site);
	
	/**
	 * 处理爬虫程序
	 * @param site
	 * @throws Exception 
	 */
	void handle(String site) throws Exception;
}
