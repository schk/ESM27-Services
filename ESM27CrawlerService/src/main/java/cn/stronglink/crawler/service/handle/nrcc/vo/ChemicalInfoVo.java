package cn.stronglink.crawler.service.handle.nrcc.vo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.stronglink.crawler.common.HtmlDataObject;
import cn.stronglink.crawler.common.HtmlKey;

/**
 * 化学品信息
 * 
 * @author yuzhantao
 *
 */
@Table(name = "chemical_nrcc")
@Entity
public class ChemicalInfoVo extends HtmlDataObject {
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	@Column(columnDefinition = "int(11) COMMENT '编号'")
	private int id;
	/**
	 * 查询用的关键字
	 */
	@Column(columnDefinition = "varchar(500) COMMENT '查询用的关键字'")
	private String queryKey;
	/**
	 * 抓取的网址
	 */
	@Column(columnDefinition = "varchar(250) COMMENT '抓取的网址'")
	private String cloneUrl;
	/**
	 * @Column(columnDefinition = "datetime COMMENT '创建日期'")
	 */
	private Date createTime;
	/**
	 * 化学品中文名
	 */
	@HtmlKey("Chemical_CNAME1")
	@Column(columnDefinition = "varchar(250) COMMENT '化学品中文名'")
	private String chemicalCNName;
	/**
	 * 其他中文名
	 */
	@HtmlKey("Chemical_CNAME2")
	@Column(columnDefinition = "varchar(250) COMMENT '其他中文名'")
	private String otherCNName;
	/**
	 * 化学品英文名
	 */
	@HtmlKey("Chemical_ENAME1")
	@Column(columnDefinition = "varchar(250) COMMENT '化学品英文名'")
	private String chemicalENName;
	/**
	 * 其他英文名
	 */
	@HtmlKey("Chemical_ENAME2")
	@Column(columnDefinition = "varchar(250) COMMENT '其他英文名'")
	private String otherENName;
	/**
	 * CAS
	 */
	@HtmlKey("Chemical_CAS")
	@Column(columnDefinition = "varchar(100) COMMENT 'CAS'")
	private String chemicalCAS;
	/**
	 * 纯品/混合物
	 */
	@HtmlKey("Chemical_ZHUCH")
	@Column(columnDefinition = "varchar(100) COMMENT '纯品/混合物'")
	private String chemicalZHUCH;
	/**
	 * 有害物成份1
	 */
	@HtmlKey("Chemical_YHCH1")
	@Column(columnDefinition = "varchar(100) COMMENT '有害物成份1'")
	private String chemicalYHCH1;
	/**
	 * 浓度1
	 */
	@HtmlKey("Chemical_YHND1")
	@Column(columnDefinition = "varchar(100) COMMENT '浓度1名'")
	private String chemicalYHND1;
	/**
	 * CAS No1
	 */
	@HtmlKey("Chemical_YHCAS1")
	@Column(columnDefinition = "varchar(100) COMMENT 'CAS No1'")
	private String chemicalYHCAS1;

	/**
	 * 有害物成份2
	 */
	@HtmlKey("Chemical_YHCH2")
	@Column(columnDefinition = "varchar(100) COMMENT '有害物成份2'")
	private String chemicalYHCH2;
	/**
	 * 浓度2
	 */
	@HtmlKey("Chemical_YHND2")
	@Column(columnDefinition = "varchar(100) COMMENT '浓度2'")
	private String chemicalYHND2;
	/**
	 * CAS No2
	 */
	@HtmlKey("Chemical_YHCAS2")
	@Column(columnDefinition = "varchar(100) COMMENT 'CAS No2'")
	private String chemicalYHCAS2;
	/**
	 * 有害物成份3
	 */
	@HtmlKey("Chemical_YHCH3")
	@Column(columnDefinition = "varchar(100) COMMENT '有害物成份3'")
	private String chemicalYHCH3;
	/**
	 * 浓度3
	 */
	@HtmlKey("Chemical_YHND3")
	@Column(columnDefinition = "varchar(100) COMMENT '浓度3'")
	private String chemicalYHND3;
	/**
	 * CAS No3
	 */
	@HtmlKey("Chemical_YHCAS3")
	@Column(columnDefinition = "varchar(100) COMMENT 'CAS No3'")
	private String chemicalYHCAS3;
	/**
	 * 有害物成份4
	 */
	@HtmlKey("Chemical_YHCH4")
	@Column(columnDefinition = "varchar(100) COMMENT '有害物成份4'")
	private String chemicalYHCH4;
	/**
	 * 浓度4
	 */
	@HtmlKey("Chemical_YHND4")
	@Column(columnDefinition = "varchar(100) COMMENT '浓度4'")
	private String chemicalYHND4;
	/**
	 * CAS No4
	 */
	@HtmlKey("Chemical_YHCAS4")
	@Column(columnDefinition = "varchar(100) COMMENT 'CAS No4'")
	private String chemicalYHCAS4;
	/**
	 * 有害物成份5
	 */
	@HtmlKey("Chemical_YHCH5")
	@Column(columnDefinition = "varchar(100) COMMENT '有害物成份5'")
	private String chemicalYHCH5;
	/**
	 * 浓度5
	 */
	@HtmlKey("Chemical_YHND5")
	@Column(columnDefinition = "varchar(100) COMMENT '浓度5'")
	private String chemicalYHND5;
	/**
	 * CAS No5
	 */
	@HtmlKey("Chemical_YHCAS5")
	@Column(columnDefinition = "varchar(100) COMMENT 'CAS No5'")
	private String chemicalYHCAS5;
	/**
	 * 危险性类别
	 */
	@HtmlKey("Chemical_CLASN")
	@Column(columnDefinition = "varchar(100) COMMENT '危险性类别'")
	private String chemicalCLASN;
	/**
	 * 侵入途径
	 */
	@HtmlKey("Chemical_QYTJ")
	@Column(columnDefinition = "varchar(100) COMMENT '侵入途径'")
	private String chemicalQYTJ;
	/**
	 * 健康危害
	 */
	@HtmlKey("Chemical_JKWH")
	@Column(columnDefinition = "varchar(500) COMMENT '健康危害'")
	private String chemicalJKWH;
	/**
	 * 环境危害
	 */
	@HtmlKey("Chemical_HJWH")
	@Column(columnDefinition = "varchar(250) COMMENT '环境危害'")
	private String chemicalHJWH;
	/**
	 * 燃爆危险
	 */
	@HtmlKey("Chemical_BRWX")
	@Column(columnDefinition = "varchar(250) COMMENT '燃爆危险'")
	private String chemicalBRWX;
	/**
	 * 皮肤接触
	 */
	@HtmlKey("Chemical_PFJC")
	@Column(columnDefinition = "varchar(250) COMMENT '皮肤接触'")
	private String chemicalPFJC;
	/**
	 * 眼睛接触
	 */
	@HtmlKey("Chemical_YJJC")
	@Column(columnDefinition = "varchar(250) COMMENT '眼睛接触'")
	private String chemicalYJJC;
	/**
	 * 吸入
	 */
	@HtmlKey("Chemical_XY")
	@Column(columnDefinition = "varchar(250) COMMENT '吸入'")
	private String chemicalXY;
	/**
	 * 食入
	 */
	@HtmlKey("Chemical_UY")
	@Column(columnDefinition = "varchar(250) COMMENT '食入'")
	private String chemicalUY;
	/**
	 * 危险特性
	 */
	@HtmlKey("Chemical_HAZN")
	@Column(columnDefinition = "varchar(500) COMMENT '危险特性'")
	private String chemicalHAZN;
	/**
	 * 有害燃烧产物
	 */
	@HtmlKey("Chemical_PRODN")
	@Column(columnDefinition = "varchar(500) COMMENT '有害燃烧产物'")
	private String chemicalPRODN;
	/**
	 * 灭火方法
	 */
	@HtmlKey("Chemical_FIRN")
	@Column(columnDefinition = "varchar(500) COMMENT '灭火方法'")
	private String chemicalFIRN;
	/**
	 * 灭火注意事项及措施
	 */
	@HtmlKey("Chemical_MHZY")
	@Column(columnDefinition = "varchar(500) COMMENT '灭火注意事项及措施'")
	private String chemicalMHZY;
	/**
	 * 应急行动
	 */
	@HtmlKey("Chemical_JJN")
	@Column(columnDefinition = "varchar(500) COMMENT '应急行动'")
	private String chemicalJJN;
	/**
	 * 操作注意事项
	 */
	@HtmlKey("Chemical_CCCZN")
	@Column(columnDefinition = "varchar(500) COMMENT '操作注意事项'")
	private String chemicalCCCZN;
	/**
	 * MAC(mg/m^3)
	 */
	@HtmlKey("Chemical_CCZYN")
	@Column(columnDefinition = "varchar(500) COMMENT 'MAC(mg/m^3)'")
	private String chemicalCCZYN;
	/**
	 * PC-TWA（mg/m^3）
	 */
	@HtmlKey("Chemical_PC_TWA")
	@Column(columnDefinition = "varchar(500) COMMENT 'PC-TWA（mg/m^3）'")
	private String ChemicalPCTWA;
	/**
	 * PC-STEL（mg/m^3）
	 */
	@HtmlKey("Chemical_PC_STEL")
	@Column(columnDefinition = "varchar(100) COMMENT 'PC-STEL（mg/m^3）'")
	private String ChemicalPCSTEL;
	/**
	 * TLV-C(mg/m^3)
	 */
	@HtmlKey("Chemical_TLV_C")
	@Column(columnDefinition = "varchar(100) COMMENT 'TLV-C(mg/m^3)'")
	private String ChemicalTLVC;
	/**
	 * TLV-TWA(mg/m^3)
	 */
	@HtmlKey("Chemical_TLV_TWA")
	@Column(columnDefinition = "varchar(100) COMMENT 'TLV-TWA(mg/m^3)'")
	private String ChemicalTLVTWA;
	/**
	 * TLV-STEL(mg/m^3)
	 */
	@HtmlKey("Chemical_TLV_STEL")
	@Column(columnDefinition = "varchar(100) COMMENT 'TLV-STEL(mg/m^3)'")
	private String chemicalTLVSTEL;
	/**
	 * 监测方法
	 */
	@HtmlKey("Chemical_JCFF")
	@Column(columnDefinition = "varchar(250) COMMENT '监测方法'")
	private String chemicalJCFF;
	/**
	 * 工程控制
	 */
	@HtmlKey("Chemical_GIKAN")
	@Column(columnDefinition = "varchar(100) COMMENT '工程控制'")
	private String chemicalGIKAN;
	/**
	 * 呼吸系统防护
	 */
	@HtmlKey("Chemical_HXFFN")
	@Column(columnDefinition = "varchar(150) COMMENT '呼吸系统防护'")
	private String chemicalHXFFN;
	/**
	 * 眼睛防护
	 */
	@HtmlKey("Chemical_YJFFN")
	@Column(columnDefinition = "varchar(100) COMMENT '眼睛防护'")
	private String chemicalYJFFN;
	/**
	 * 身体防护
	 */
	@HtmlKey("Chemical_FFHN")
	@Column(columnDefinition = "varchar(100) COMMENT '身体防护'")
	private String chemicalFFHN;
	/**
	 * 手 防 护
	 */
	@HtmlKey("Chemical_UFHN")
	@Column(columnDefinition = "varchar(100) COMMENT '手防护'")
	private String chemicalUFHN;
	/**
	 * 其他防护
	 */
	@HtmlKey("Chemical_QTFFN")
	@Column(columnDefinition = "varchar(100) COMMENT '其他防护'")
	private String chemicalQTFFN;
	/**
	 * 外观与性状
	 */
	@HtmlKey("Chemical_STATE")
	@Column(columnDefinition = "varchar(50) COMMENT '外观与性状'")
	private String chemicalSTATE;
	/**
	 * pH值
	 */
	@HtmlKey("Chemical_PH")
	@Column(columnDefinition = "varchar(50) COMMENT 'pH值'")
	private String chemicalPH;
	/**
	 * 熔点(℃)
	 */
	@HtmlKey("Chemical_MP")
	@Column(columnDefinition = "varchar(50) COMMENT '熔点(℃)'")
	private String chemicalMP;
	/**
	 * 沸点(℃)
	 */
	@HtmlKey("chemicalBP")
	@Column(columnDefinition = "varchar(50) COMMENT '沸点(℃)'")
	private String Chemical_BP;
	/**
	 * 相对密度(水=1)
	 */
	@HtmlKey("Chemical_D4")
	@Column(columnDefinition = "varchar(30) COMMENT '相对密度(水=1)'")
	private String Chemical_D4;
	/**
	 * 相对蒸气密度(空气=1)
	 */
	@HtmlKey("Chemical_VD")
	@Column(columnDefinition = "varchar(50) COMMENT '相对蒸气密度(空气=1)'")
	private String chemicalVD;
	/**
	 * 饱和蒸气压(kPa)
	 */
	@HtmlKey("Chemical_VP")
	@Column(columnDefinition = "varchar(50) COMMENT '饱和蒸气压(kPa)'")
	private String chemicalVP;
	/**
	 * 燃烧热(kJ/mol)
	 */
	@HtmlKey("Chemical_RSRZ")
	@Column(columnDefinition = "varchar(50) COMMENT '燃烧热(kJ/mol)'")
	private String chemicalRSRZ;
	/**
	 * 临界温度(℃)
	 */
	@HtmlKey("Chemical_LJWD")
	@Column(columnDefinition = "varchar(50) COMMENT '临界温度(℃)'")
	private String chemicalLJWD;
	/**
	 * 临界压力(Mpa)
	 */
	@HtmlKey("Chemical_LJYL")
	@Column(columnDefinition = "varchar(50) COMMENT '临界压力(Mpa)'")
	private String chemicalLJYL;
	/**
	 * 辛醇/水分配系数
	 */
	@HtmlKey("Chemical_XCFP")
	@Column(columnDefinition = "varchar(50) COMMENT '辛醇/水分配系数'")
	private String chemicalXCFP;
	/**
	 * 闪点(℃)
	 */
	@HtmlKey("Chemical_FLS")
	@Column(columnDefinition = "varchar(50) COMMENT '闪点(℃)'")
	private String chemicalFLS;
	/**
	 * 引燃温度(℃)
	 */
	@HtmlKey("Chemical_TEM")
	@Column(columnDefinition = "varchar(50) COMMENT '引燃温度(℃)'")
	private String chemicalTEM;
	/**
	 * 爆炸下限[％(V/V)]
	 */
	@HtmlKey("Chemical_LIM1")
	@Column(columnDefinition = "varchar(50) COMMENT '爆炸下限[％(V/V)]'")
	private String chemicalLIM1;
	/**
	 * 爆炸上限[％(V/V)]
	 */
	@HtmlKey("Chemical_LIM2")
	@Column(columnDefinition = "varchar(50) COMMENT '爆炸上限[％(V/V)]'")
	private String chemicalLIM2;
	/**
	 * 溶解性
	 */
	@HtmlKey("Chemical_RJXN")
	@Column(columnDefinition = "varchar(500) COMMENT '溶解性'")
	private String chemicalRJXN;
	/**
	 * 主要用途
	 */
	@HtmlKey("Chemical_USED")
	@Column(columnDefinition = "varchar(500) COMMENT '主要用途'")
	private String chemicalUSED;
	/**
	 * 稳定性
	 */
	@HtmlKey("Chemical_STAB")
	@Column(columnDefinition = "varchar(250) COMMENT '稳定性'")
	private String chemicalSTAB;
	/**
	 * 禁配物
	 */
	@HtmlKey("Chemical_CAS")
	@Column(columnDefinition = "varchar(250) COMMENT '禁配物'")
	private String Chemical_IMPN;
	/**
	 * 避免接触的条件
	 */
	@HtmlKey("Chemical_COD")
	@Column(columnDefinition = "varchar(500) COMMENT '避免接触的条件'")
	private String chemicalCOD;
	/**
	 * 聚合危害
	 */
	@HtmlKey("Chemical_POL")
	@Column(columnDefinition = "varchar(500) COMMENT '聚合危害'")
	private String chemicalPOL;
	/**
	 * 分解产物
	 */
	@HtmlKey("Chemical_FJCW")
	@Column(columnDefinition = "varchar(500) COMMENT '分解产物'")
	private String chemicalFJCW;
	/**
	 * 废弃物性质
	 */
	@HtmlKey("Chemical_FQNXZ")
	@Column(columnDefinition = "varchar(100) COMMENT '废弃物性质'")
	private String chemicalFQNXZ;
	/**
	 * 废弃处置方法
	 */
	@HtmlKey("Chemical_FQN")
	@Column(columnDefinition = "varchar(500) COMMENT '废弃处置方法'")
	private String chemicalFQN;
	/**
	 * 废弃注意事项
	 */
	@HtmlKey("Chemical_FQNZY")
	@Column(columnDefinition = "varchar(500) COMMENT '废弃注意事项'")
	private String chemicalFQNZY;
	/**
	 * 危险货物编号
	 */
	@HtmlKey("Chemical_WGH")
	@Column(columnDefinition = "varchar(100) COMMENT '危险货物编号'")
	private String chemicalWGH;
	/**
	 * UN编号
	 */
	@HtmlKey("Chemical_UN")
	@Column(columnDefinition = "varchar(100) COMMENT 'UN编号'")
	private String chemicalUN;
	/**
	 * 包装类别
	 */
	@HtmlKey("Chemical_BZFLT")
	@Column(columnDefinition = "varchar(50) COMMENT '包装类别'")
	private String chemicalBZFLT;
	/**
	 * 包装标志
	 */
	@HtmlKey("Chemical_MARKT")
	@Column(columnDefinition = "varchar(500) COMMENT '包装标志'")
	private String chemicalMARKT;
	/**
	 * 包装方法
	 */
	@HtmlKey("Chemical_BZFFT")
	@Column(columnDefinition = "varchar(500) COMMENT '包装方法'")
	private String chemicalBZFFT;
	/**
	 * 运输注意事项
	 */
	@HtmlKey("Chemical_YSZYN")
	@Column(columnDefinition = "varchar(500) COMMENT '运输注意事项'")
	private String chemicalYSZYN;
	/**
	 * 法规信息
	 */
	@HtmlKey("Chemical_FGXXN")
	@Column(columnDefinition = "varchar(500) COMMENT '法规信息'")
	private String chemicalFGXXN;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQueryKey() {
		return queryKey;
	}
	public void setQueryKey(String queryKey) {
		this.queryKey = queryKey;
	}
	public String getCloneUrl() {
		return cloneUrl;
	}
	public void setCloneUrl(String cloneUrl) {
		this.cloneUrl = cloneUrl;
	}
	public String getChemicalCNName() {
		return chemicalCNName;
	}
	public void setChemicalCNName(String chemicalCNName) {
		this.chemicalCNName = chemicalCNName;
	}
	public String getOtherCNName() {
		return otherCNName;
	}
	public void setOtherCNName(String otherCNName) {
		this.otherCNName = otherCNName;
	}
	public String getChemicalENName() {
		return chemicalENName;
	}
	public void setChemicalENName(String chemicalENName) {
		this.chemicalENName = chemicalENName;
	}
	public String getOtherENName() {
		return otherENName;
	}
	public void setOtherENName(String otherENName) {
		this.otherENName = otherENName;
	}
	public String getChemicalCAS() {
		return chemicalCAS;
	}
	public void setChemicalCAS(String chemicalCAS) {
		this.chemicalCAS = chemicalCAS;
	}
	public String getChemicalZHUCH() {
		return chemicalZHUCH;
	}
	public void setChemicalZHUCH(String chemicalZHUCH) {
		this.chemicalZHUCH = chemicalZHUCH;
	}
	public String getChemicalYHCH1() {
		return chemicalYHCH1;
	}
	public void setChemicalYHCH1(String chemicalYHCH1) {
		this.chemicalYHCH1 = chemicalYHCH1;
	}
	public String getChemicalYHND1() {
		return chemicalYHND1;
	}
	public void setChemicalYHND1(String chemicalYHND1) {
		this.chemicalYHND1 = chemicalYHND1;
	}
	public String getChemicalYHCAS1() {
		return chemicalYHCAS1;
	}
	public void setChemicalYHCAS1(String chemicalYHCAS1) {
		this.chemicalYHCAS1 = chemicalYHCAS1;
	}
	public String getChemicalYHCH2() {
		return chemicalYHCH2;
	}
	public void setChemicalYHCH2(String chemicalYHCH2) {
		this.chemicalYHCH2 = chemicalYHCH2;
	}
	public String getChemicalYHND2() {
		return chemicalYHND2;
	}
	public void setChemicalYHND2(String chemicalYHND2) {
		this.chemicalYHND2 = chemicalYHND2;
	}
	public String getChemicalYHCAS2() {
		return chemicalYHCAS2;
	}
	public void setChemicalYHCAS2(String chemicalYHCAS2) {
		this.chemicalYHCAS2 = chemicalYHCAS2;
	}
	public String getChemicalYHCH3() {
		return chemicalYHCH3;
	}
	public void setChemicalYHCH3(String chemicalYHCH3) {
		this.chemicalYHCH3 = chemicalYHCH3;
	}
	public String getChemicalYHND3() {
		return chemicalYHND3;
	}
	public void setChemicalYHND3(String chemicalYHND3) {
		this.chemicalYHND3 = chemicalYHND3;
	}
	public String getChemicalYHCAS3() {
		return chemicalYHCAS3;
	}
	public void setChemicalYHCAS3(String chemicalYHCAS3) {
		this.chemicalYHCAS3 = chemicalYHCAS3;
	}
	public String getChemicalYHCH4() {
		return chemicalYHCH4;
	}
	public void setChemicalYHCH4(String chemicalYHCH4) {
		this.chemicalYHCH4 = chemicalYHCH4;
	}
	public String getChemicalYHND4() {
		return chemicalYHND4;
	}
	public void setChemicalYHND4(String chemicalYHND4) {
		this.chemicalYHND4 = chemicalYHND4;
	}
	public String getChemicalYHCAS4() {
		return chemicalYHCAS4;
	}
	public void setChemicalYHCAS4(String chemicalYHCAS4) {
		this.chemicalYHCAS4 = chemicalYHCAS4;
	}
	public String getChemicalYHCH5() {
		return chemicalYHCH5;
	}
	public void setChemicalYHCH5(String chemicalYHCH5) {
		this.chemicalYHCH5 = chemicalYHCH5;
	}
	public String getChemicalYHND5() {
		return chemicalYHND5;
	}
	public void setChemicalYHND5(String chemicalYHND5) {
		this.chemicalYHND5 = chemicalYHND5;
	}
	public String getChemicalYHCAS5() {
		return chemicalYHCAS5;
	}
	public void setChemicalYHCAS5(String chemicalYHCAS5) {
		this.chemicalYHCAS5 = chemicalYHCAS5;
	}
	public String getChemicalCLASN() {
		return chemicalCLASN;
	}
	public void setChemicalCLASN(String chemicalCLASN) {
		this.chemicalCLASN = chemicalCLASN;
	}
	public String getChemicalQYTJ() {
		return chemicalQYTJ;
	}
	public void setChemicalQYTJ(String chemicalQYTJ) {
		this.chemicalQYTJ = chemicalQYTJ;
	}
	public String getChemicalJKWH() {
		return chemicalJKWH;
	}
	public void setChemicalJKWH(String chemicalJKWH) {
		this.chemicalJKWH = chemicalJKWH;
	}
	public String getChemicalHJWH() {
		return chemicalHJWH;
	}
	public void setChemicalHJWH(String chemicalHJWH) {
		this.chemicalHJWH = chemicalHJWH;
	}
	public String getChemicalBRWX() {
		return chemicalBRWX;
	}
	public void setChemicalBRWX(String chemicalBRWX) {
		this.chemicalBRWX = chemicalBRWX;
	}
	public String getChemicalPFJC() {
		return chemicalPFJC;
	}
	public void setChemicalPFJC(String chemicalPFJC) {
		this.chemicalPFJC = chemicalPFJC;
	}
	public String getChemicalYJJC() {
		return chemicalYJJC;
	}
	public void setChemicalYJJC(String chemicalYJJC) {
		this.chemicalYJJC = chemicalYJJC;
	}
	public String getChemicalXY() {
		return chemicalXY;
	}
	public void setChemicalXY(String chemicalXY) {
		this.chemicalXY = chemicalXY;
	}
	public String getChemicalUY() {
		return chemicalUY;
	}
	public void setChemicalUY(String chemicalUY) {
		this.chemicalUY = chemicalUY;
	}
	public String getChemicalHAZN() {
		return chemicalHAZN;
	}
	public void setChemicalHAZN(String chemicalHAZN) {
		this.chemicalHAZN = chemicalHAZN;
	}
	public String getChemicalPRODN() {
		return chemicalPRODN;
	}
	public void setChemicalPRODN(String chemicalPRODN) {
		this.chemicalPRODN = chemicalPRODN;
	}
	public String getChemicalFIRN() {
		return chemicalFIRN;
	}
	public void setChemicalFIRN(String chemicalFIRN) {
		this.chemicalFIRN = chemicalFIRN;
	}
	public String getChemicalMHZY() {
		return chemicalMHZY;
	}
	public void setChemicalMHZY(String chemicalMHZY) {
		this.chemicalMHZY = chemicalMHZY;
	}
	public String getChemicalJJN() {
		return chemicalJJN;
	}
	public void setChemicalJJN(String chemicalJJN) {
		this.chemicalJJN = chemicalJJN;
	}
	public String getChemicalCCCZN() {
		return chemicalCCCZN;
	}
	public void setChemicalCCCZN(String chemicalCCCZN) {
		this.chemicalCCCZN = chemicalCCCZN;
	}
	public String getChemicalCCZYN() {
		return chemicalCCZYN;
	}
	public void setChemicalCCZYN(String chemicalCCZYN) {
		this.chemicalCCZYN = chemicalCCZYN;
	}
	public String getChemicalPCTWA() {
		return ChemicalPCTWA;
	}
	public void setChemicalPCTWA(String chemicalPCTWA) {
		ChemicalPCTWA = chemicalPCTWA;
	}
	public String getChemicalPCSTEL() {
		return ChemicalPCSTEL;
	}
	public void setChemicalPCSTEL(String chemicalPCSTEL) {
		ChemicalPCSTEL = chemicalPCSTEL;
	}
	public String getChemicalTLVC() {
		return ChemicalTLVC;
	}
	public void setChemicalTLVC(String chemicalTLVC) {
		ChemicalTLVC = chemicalTLVC;
	}
	public String getChemicalTLVTWA() {
		return ChemicalTLVTWA;
	}
	public void setChemicalTLVTWA(String chemicalTLVTWA) {
		ChemicalTLVTWA = chemicalTLVTWA;
	}
	public String getChemicalTLVSTEL() {
		return chemicalTLVSTEL;
	}
	public void setChemicalTLVSTEL(String chemicalTLVSTEL) {
		this.chemicalTLVSTEL = chemicalTLVSTEL;
	}
	public String getChemicalJCFF() {
		return chemicalJCFF;
	}
	public void setChemicalJCFF(String chemicalJCFF) {
		this.chemicalJCFF = chemicalJCFF;
	}
	public String getChemicalGIKAN() {
		return chemicalGIKAN;
	}
	public void setChemicalGIKAN(String chemicalGIKAN) {
		this.chemicalGIKAN = chemicalGIKAN;
	}
	public String getChemicalHXFFN() {
		return chemicalHXFFN;
	}
	public void setChemicalHXFFN(String chemicalHXFFN) {
		this.chemicalHXFFN = chemicalHXFFN;
	}
	public String getChemicalYJFFN() {
		return chemicalYJFFN;
	}
	public void setChemicalYJFFN(String chemicalYJFFN) {
		this.chemicalYJFFN = chemicalYJFFN;
	}
	public String getChemicalFFHN() {
		return chemicalFFHN;
	}
	public void setChemicalFFHN(String chemicalFFHN) {
		this.chemicalFFHN = chemicalFFHN;
	}
	public String getChemicalUFHN() {
		return chemicalUFHN;
	}
	public void setChemicalUFHN(String chemicalUFHN) {
		this.chemicalUFHN = chemicalUFHN;
	}
	public String getChemicalQTFFN() {
		return chemicalQTFFN;
	}
	public void setChemicalQTFFN(String chemicalQTFFN) {
		this.chemicalQTFFN = chemicalQTFFN;
	}
	public String getChemicalSTATE() {
		return chemicalSTATE;
	}
	public void setChemicalSTATE(String chemicalSTATE) {
		this.chemicalSTATE = chemicalSTATE;
	}
	public String getChemicalPH() {
		return chemicalPH;
	}
	public void setChemicalPH(String chemicalPH) {
		this.chemicalPH = chemicalPH;
	}
	public String getChemicalMP() {
		return chemicalMP;
	}
	public void setChemicalMP(String chemicalMP) {
		this.chemicalMP = chemicalMP;
	}
	public String getChemical_BP() {
		return Chemical_BP;
	}
	public void setChemical_BP(String chemical_BP) {
		Chemical_BP = chemical_BP;
	}
	public String getChemical_D4() {
		return Chemical_D4;
	}
	public void setChemical_D4(String chemical_D4) {
		Chemical_D4 = chemical_D4;
	}
	public String getChemicalVD() {
		return chemicalVD;
	}
	public void setChemicalVD(String chemicalVD) {
		this.chemicalVD = chemicalVD;
	}
	public String getChemicalVP() {
		return chemicalVP;
	}
	public void setChemicalVP(String chemicalVP) {
		this.chemicalVP = chemicalVP;
	}
	public String getChemicalRSRZ() {
		return chemicalRSRZ;
	}
	public void setChemicalRSRZ(String chemicalRSRZ) {
		this.chemicalRSRZ = chemicalRSRZ;
	}
	public String getChemicalLJWD() {
		return chemicalLJWD;
	}
	public void setChemicalLJWD(String chemicalLJWD) {
		this.chemicalLJWD = chemicalLJWD;
	}
	public String getChemicalLJYL() {
		return chemicalLJYL;
	}
	public void setChemicalLJYL(String chemicalLJYL) {
		this.chemicalLJYL = chemicalLJYL;
	}
	public String getChemicalXCFP() {
		return chemicalXCFP;
	}
	public void setChemicalXCFP(String chemicalXCFP) {
		this.chemicalXCFP = chemicalXCFP;
	}
	public String getChemicalFLS() {
		return chemicalFLS;
	}
	public void setChemicalFLS(String chemicalFLS) {
		this.chemicalFLS = chemicalFLS;
	}
	public String getChemicalTEM() {
		return chemicalTEM;
	}
	public void setChemicalTEM(String chemicalTEM) {
		this.chemicalTEM = chemicalTEM;
	}
	public String getChemicalLIM1() {
		return chemicalLIM1;
	}
	public void setChemicalLIM1(String chemicalLIM1) {
		this.chemicalLIM1 = chemicalLIM1;
	}
	public String getChemicalLIM2() {
		return chemicalLIM2;
	}
	public void setChemicalLIM2(String chemicalLIM2) {
		this.chemicalLIM2 = chemicalLIM2;
	}
	public String getChemicalRJXN() {
		return chemicalRJXN;
	}
	public void setChemicalRJXN(String chemicalRJXN) {
		this.chemicalRJXN = chemicalRJXN;
	}
	public String getChemicalUSED() {
		return chemicalUSED;
	}
	public void setChemicalUSED(String chemicalUSED) {
		this.chemicalUSED = chemicalUSED;
	}
	public String getChemicalSTAB() {
		return chemicalSTAB;
	}
	public void setChemicalSTAB(String chemicalSTAB) {
		this.chemicalSTAB = chemicalSTAB;
	}
	public String getChemical_IMPN() {
		return Chemical_IMPN;
	}
	public void setChemical_IMPN(String chemical_IMPN) {
		Chemical_IMPN = chemical_IMPN;
	}
	public String getChemicalCOD() {
		return chemicalCOD;
	}
	public void setChemicalCOD(String chemicalCOD) {
		this.chemicalCOD = chemicalCOD;
	}
	public String getChemicalPOL() {
		return chemicalPOL;
	}
	public void setChemicalPOL(String chemicalPOL) {
		this.chemicalPOL = chemicalPOL;
	}
	public String getChemicalFJCW() {
		return chemicalFJCW;
	}
	public void setChemicalFJCW(String chemicalFJCW) {
		this.chemicalFJCW = chemicalFJCW;
	}
	public String getChemicalFQNXZ() {
		return chemicalFQNXZ;
	}
	public void setChemicalFQNXZ(String chemicalFQNXZ) {
		this.chemicalFQNXZ = chemicalFQNXZ;
	}
	public String getChemicalFQN() {
		return chemicalFQN;
	}
	public void setChemicalFQN(String chemicalFQN) {
		this.chemicalFQN = chemicalFQN;
	}
	public String getChemicalFQNZY() {
		return chemicalFQNZY;
	}
	public void setChemicalFQNZY(String chemicalFQNZY) {
		this.chemicalFQNZY = chemicalFQNZY;
	}
	public String getChemicalWGH() {
		return chemicalWGH;
	}
	public void setChemicalWGH(String chemicalWGH) {
		this.chemicalWGH = chemicalWGH;
	}
	public String getChemicalUN() {
		return chemicalUN;
	}
	public void setChemicalUN(String chemicalUN) {
		this.chemicalUN = chemicalUN;
	}
	public String getChemicalBZFLT() {
		return chemicalBZFLT;
	}
	public void setChemicalBZFLT(String chemicalBZFLT) {
		this.chemicalBZFLT = chemicalBZFLT;
	}
	public String getChemicalMARKT() {
		return chemicalMARKT;
	}
	public void setChemicalMARKT(String chemicalMARKT) {
		this.chemicalMARKT = chemicalMARKT;
	}
	public String getChemicalBZFFT() {
		return chemicalBZFFT;
	}
	public void setChemicalBZFFT(String chemicalBZFFT) {
		this.chemicalBZFFT = chemicalBZFFT;
	}
	public String getChemicalYSZYN() {
		return chemicalYSZYN;
	}
	public void setChemicalYSZYN(String chemicalYSZYN) {
		this.chemicalYSZYN = chemicalYSZYN;
	}
	public String getChemicalFGXXN() {
		return chemicalFGXXN;
	}
	public void setChemicalFGXXN(String chemicalFGXXN) {
		this.chemicalFGXXN = chemicalFGXXN;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
