package cn.stronglink.crawler.service.handle.nrcc.vo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChemicalInfoVoRepository extends JpaRepository<ChemicalInfoVo,Integer> {

}
