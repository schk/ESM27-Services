package cn.stronglink.crawler.vo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 爬虫操作相关日志
 * @author yuzhantao
 *
 */
@Table(name="CHEMICAL_LOG")
@Entity
public class ChemicalLogVo {
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	@Column(columnDefinition="int(11) COMMENT '编号'")
	private int id;
	@Column(columnDefinition="varchar(250) COMMENT '抓取的网址'")
	private String site;
	@Column(columnDefinition="varchar(250) COMMENT '日志信息'")
	private String messge;
	@Column(columnDefinition="datetime COMMENT '创建时间'")
	private Date createTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getMessge() {
		return messge;
	}
	public void setMessge(String messge) {
		this.messge = messge;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	
}
