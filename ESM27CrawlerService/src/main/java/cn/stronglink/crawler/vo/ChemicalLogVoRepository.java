package cn.stronglink.crawler.vo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ChemicalLogVoRepository extends JpaRepository<ChemicalLogVo,Integer> {

}
