package cn.stronglink.crawler.vo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 标准气体对象
 * @author yuzhantao
 *
 */
@Table(name="CHEMICAL")
@Entity
public class ChemicalVo {
	@Id
	private int id;
	private String chemicalCnName;
	private String chemicalEnName;
	private String srcUrl;
	private String tableName;
	private String content;
	private Date createTime;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getChemicalCnName() {
		return chemicalCnName;
	}
	public void setChemicalCnName(String chemicalCnName) {
		this.chemicalCnName = chemicalCnName;
	}
	public String getChemicalEnName() {
		return chemicalEnName;
	}
	public void setChemicalEnName(String chemicalEnName) {
		this.chemicalEnName = chemicalEnName;
	}
	public String getSrcUrl() {
		return srcUrl;
	}
	public void setSrcUrl(String srcUrl) {
		this.srcUrl = srcUrl;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	
}
