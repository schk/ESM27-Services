package cn.stronglink.crawler.vo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ChemicalVoRepository extends JpaRepository<ChemicalVo, Integer> {
	@Query(value = "SELECT id, chemicalcnname AS chemical_cn_name, chemicalenname AS chemical_en_name, clone_url AS src_url, 'nrcc' AS table_name, CONCAT( '纯品/混合物:', IFNULL(chemicalzhuch, ''), '    有害物成份:', IFNULL(chemicalyhch1, ''), '    CAS:', IFNULL(chemicalcas, ''),'    危险性类别',chemicalCLASN,'    侵入途径',chemicalQYTJ,'    健康危害',chemicalJKWH,'    环境危害',chemicalHJWH,'    燃爆危险',chemicalBRWX,'    皮肤接触',chemicalPFJC) AS content, create_time, query_key FROM chemical_nrcc where query_key like %?1% ORDER BY ?#{#page}", 
			countQuery = "SELECT count(*) FROM chemical_nrcc where query_key like %?1%", 
			nativeQuery = true)
	Page<ChemicalVo> search(String key,Pageable page);
}
