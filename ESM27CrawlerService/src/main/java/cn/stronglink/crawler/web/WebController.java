package cn.stronglink.crawler.web;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.util.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.Service.Listener;
import com.google.common.util.concurrent.Service.State;

import cn.stronglink.crawler.service.ChemicalService;
import cn.stronglink.crawler.service.CrawlerService;
import cn.stronglink.crawler.service.handle.nrcc.vo.ChemicalInfoVo;
import cn.stronglink.crawler.vo.ChemicalVo;

@Controller
@RequestMapping("/")
public class WebController {
	private final static Logger logger = LogManager.getLogger(WebController.class);
	@Autowired
	private CrawlerService crawlerService; // 爬虫服务类
	@Autowired
	private ChemicalService chemicalService; // 危化品服务类

	@RequestMapping("/")
	public String home() {
		return "index";
	}

	/**
	 * 气体列表
	 * 
	 * @param model
	 * @param key
	 * @param page
	 * @param size
	 * @return
	 */
	@RequestMapping(value = "chemical", method = RequestMethod.GET)
	public String list(Model model, @RequestParam(value = "key", defaultValue = "") String key,
			@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "size", defaultValue = "10") Integer size) {
		Page<ChemicalVo> chemicals = chemicalService.search(key, page, size);
		logger.info("查询到的化学品列表 JSON={}", JSON.toJSONString(chemicals));
		model.addAttribute("page", chemicals);
		model.addAttribute("key", key);
		return "chemical/list";
	}

	/**
	 * 气体详情
	 * 
	 * @param model
	 * @param key
	 * @param page
	 * @param size
	 * @return
	 */
	@RequestMapping(value = "chemical/{type}/{id}", method = RequestMethod.GET)
	public String chemicalDetail(Model model, @PathVariable("type") String type, @PathVariable("id") String id) {
		Preconditions.checkNotNull(id);
		if (type.equals("nrcc")) {
			Object chemical = (ChemicalInfoVo) chemicalService.find(Integer.valueOf(id));
			logger.info("查询到的资产对象 JSON={}", JSON.toJSONString(chemical));
			model.addAttribute("chemical", chemical);
		}
		return "chemical/detail";
	}

	@ResponseBody
	@RequestMapping("nrcc")
	public String downloadOfNrccSite() {
		crawlerService.setSite("http://service.nrcc.com.cn/Mobile/MyQuickWatch");

		crawlerService.addListener(new Listener() {
			@Override
			public void starting() {
				System.out.println("爬虫服务开始启动.....");
			}

			@Override
			public void running() {
				System.out.println("爬虫服务开始运行");
				;
			}

			@Override
			public void stopping(State from) {
				System.out.println("爬虫服务关闭中");
			}

			@Override
			public void terminated(State from) {
				System.out.println("爬虫服务终止");
			}

			@Override
			public void failed(State from, Throwable failure) {
				System.out.println("爬虫服务运行失败:" + failure.getCause());
			}
		}, MoreExecutors.directExecutor());

		crawlerService.startAsync().awaitRunning();
		return "";
	}
}
