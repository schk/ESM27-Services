package com.stronglink.esm27.datasync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Esm27DataSyncService1Application {

	public static void main(String[] args) {
		SpringApplication.run(Esm27DataSyncService1Application.class, args);
	}
}
