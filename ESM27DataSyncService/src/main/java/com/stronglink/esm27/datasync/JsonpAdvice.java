package com.stronglink.esm27.datasync;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

@ControllerAdvice(basePackages = "com.stronglink.esm27.datasync")
@SuppressWarnings("deprecation")
public class JsonpAdvice extends AbstractJsonpResponseBodyAdvice {
	public JsonpAdvice() { 
        super("callback","jsonp");
    }
}
