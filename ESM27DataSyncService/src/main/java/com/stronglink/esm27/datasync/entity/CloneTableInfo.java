package com.stronglink.esm27.datasync.entity;

public class CloneTableInfo {
	private int count;
	private String tableName;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
}
