package com.stronglink.esm27.datasync.entity;

public class ResultTableEntity {
	private String token;
	private String tableName;
	private String message;
	private String state;
	
	public ResultTableEntity(String token,String tableName,String message,String state) {
		this.token=token;
		this.tableName=tableName;
		this.message=message;
		this.state=state;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
}
