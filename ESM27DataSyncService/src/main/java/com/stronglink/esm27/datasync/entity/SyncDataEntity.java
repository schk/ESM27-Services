package com.stronglink.esm27.datasync.entity;

/**
 * 同步数据实例
 * @author yuzhantao
 *
 */
public class SyncDataEntity {
	/**
	 * 源数据源参数
	 */
	private String srcDataSourceParams;
	/**
	 * 目标数据源参数
	 */
	private String destDataSourceParams;
	/**
	 * 用来识别表中同步字段的名称
	 */
	private SyncTableEntity[] syncTables;
	
	private String srcFtpHost;
	
	private int srcFtpPort;
	
	private String srcFtpLoginName;
	
	private String srcFtpLoginPassword;
	
	private String destPath;
	
	private String token;
	
	/**
	 * 同步模式
	 */
	private String syncMode;
	
	public String getSrcDataSourceParams() {
		return srcDataSourceParams;
	}
	public void setSrcDataSourceParams(String srcDataSourceParams) {
		this.srcDataSourceParams = srcDataSourceParams;
	}
	public String getDestDataSourceParams() {
		return destDataSourceParams;
	}
	public void setDestDataSourceParams(String destDataSourceParams) {
		this.destDataSourceParams = destDataSourceParams;
	}
	public SyncTableEntity[] getSyncTables() {
		return syncTables;
	}
	public void setSyncTables(SyncTableEntity[] syncTables) {
		this.syncTables = syncTables;
	}
	public String getSrcFtpLoginName() {
		return srcFtpLoginName;
	}
	public void setSrcFtpLoginName(String srcFtpLoginName) {
		this.srcFtpLoginName = srcFtpLoginName;
	}
	public String getSrcFtpLoginPassword() {
		return srcFtpLoginPassword;
	}
	public void setSrcFtpLoginPassword(String srcFtpLoginPassword) {
		this.srcFtpLoginPassword = srcFtpLoginPassword;
	}
	public String getSrcFtpHost() {
		return srcFtpHost;
	}
	public void setSrcFtpHost(String srcFtpHost) {
		this.srcFtpHost = srcFtpHost;
	}
	public int getSrcFtpPort() {
		return srcFtpPort;
	}
	public void setSrcFtpPort(int srcFtpPort) {
		this.srcFtpPort = srcFtpPort;
	}
	public String getDestPath() {
		return destPath;
	}
	public void setDestPath(String destPath) {
		this.destPath = destPath;
	}
	public String getSyncMode() {
		return syncMode;
	}
	public void setSyncMode(String syncMode) {
		this.syncMode = syncMode;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
}
