package com.stronglink.esm27.datasync.entity;

public class SyncTableEntity {
	/**
	 * 表名
	 */
	private String tableName;
	/**
	 * 需要同步的文件字段数组
	 */
	private String[] snycFileColumnNames;
	
	/**
	 * 同步更新用的字段名称
	 */
	private String syncUpdateColumnName;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String[] getSnycFileColumnNames() {
		return snycFileColumnNames;
	}

	public void setSnycFileColumnNames(String[] snycFileColumnNames) {
		this.snycFileColumnNames = snycFileColumnNames;
	}

	public String getSyncUpdateColumnName() {
		return syncUpdateColumnName;
	}

	public void setSyncUpdateColumnName(String syncUpdateColumnName) {
		this.syncUpdateColumnName = syncUpdateColumnName;
	}
}
