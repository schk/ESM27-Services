package com.stronglink.esm27.datasync.entity;

import java.util.Map;
import java.util.Set;

/**
 * 同步表的结构
 * @author yuzhantao
 *
 */
public class SyncTableStructureEntity {
	Map<String, Set<String>> srcTableStructure;
	Map<String, Set<String>> destTableStructure;
	Map<String, SyncTableEntity> syncTableParams;
	
	public Map<String, Set<String>> getSrcTableStructure() {
		return srcTableStructure;
	}
	public void setSrcTableStructure(Map<String, Set<String>> srcTableStructure) {
		this.srcTableStructure = srcTableStructure;
	}
	public Map<String, Set<String>> getDestTableStructure() {
		return destTableStructure;
	}
	public void setDestTableStructure(Map<String, Set<String>> destTableStructure) {
		this.destTableStructure = destTableStructure;
	}
	public Map<String, SyncTableEntity> getSyncTableParams() {
		return syncTableParams;
	}
	public void setSyncTableParams(Map<String, SyncTableEntity> syncTableParams) {
		this.syncTableParams = syncTableParams;
	}
}
