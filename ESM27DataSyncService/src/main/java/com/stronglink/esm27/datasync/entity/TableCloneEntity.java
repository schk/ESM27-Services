package com.stronglink.esm27.datasync.entity;

/**
 * 克隆表格需要的参数实体
 * @author yuzhantao
 *
 */
public class TableCloneEntity {
	/**
	 * 源数据源参数
	 */
	private String srcDataSourceParams;
	/**
	 * 目标数据源参数
	 */
	private String destDataSourceParams;
	/**
	 * 需要克隆的表数组
	 */
	private String[] cloneTables;
	public String getSrcDataSourceParams() {
		return srcDataSourceParams;
	}
	public void setSrcDataSourceParams(String srcDataSourceParams) {
		this.srcDataSourceParams = srcDataSourceParams;
	}
	public String getDestDataSourceParams() {
		return destDataSourceParams;
	}
	public void setDestDataSourceParams(String destDataSourceParams) {
		this.destDataSourceParams = destDataSourceParams;
	}
	public String[] getCloneTables() {
		return cloneTables;
	}
	public void setCloneTables(String[] cloneTables) {
		this.cloneTables = cloneTables;
	}
}
