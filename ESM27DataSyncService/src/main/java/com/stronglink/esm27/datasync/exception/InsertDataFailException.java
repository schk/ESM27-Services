package com.stronglink.esm27.datasync.exception;

/**
 * 插入数据失败
 * @author yuzhantao
 *
 */
public class InsertDataFailException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InsertDataFailException(String sql) {
		super("插入sql数据失败:"+sql);
	}
}
