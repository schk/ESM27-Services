package com.stronglink.esm27.datasync.exception;

/**
 * 没有匹配表格的异常
 * @author yuzhantao
 *
 */
public class NoMatchingTableException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5994743997467280189L;

}
