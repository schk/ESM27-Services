package com.stronglink.esm27.datasync.exception;

/**
 * 同步任务已存在的异常
 * @author yuzhantao
 *
 */
public class SyncExistedException extends Exception {

}
