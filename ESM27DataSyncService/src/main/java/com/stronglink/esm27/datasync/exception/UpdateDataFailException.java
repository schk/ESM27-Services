package com.stronglink.esm27.datasync.exception;

public class UpdateDataFailException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UpdateDataFailException(String sql) {
		super("更新sql数据失败:"+sql);
	}
}
