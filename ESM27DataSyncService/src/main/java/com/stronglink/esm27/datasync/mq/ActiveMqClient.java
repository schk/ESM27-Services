package com.stronglink.esm27.datasync.mq;

import javax.jms.Destination;

import org.apache.activemq.command.ActiveMQTopic;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Service;

@Service(value = "activeMqClient")
public class ActiveMqClient {
	@Autowired
	private JmsMessagingTemplate jmsTemplate;

	private Logger logger = LogManager.getLogger(ActiveMqClient.class);

	public void send(String topic, String msg) {
		Destination destination = new ActiveMQTopic(topic);
		jmsTemplate.convertAndSend(destination, msg);
		logger.info("发送MQ Topic=" + destination + ",  Content=" + msg);
	}
}
