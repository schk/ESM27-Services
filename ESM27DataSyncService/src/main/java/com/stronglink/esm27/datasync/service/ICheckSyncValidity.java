package com.stronglink.esm27.datasync.service;

/**
 * 检查同步的有效性
 * @author yuzhantao
 *
 */
public interface ICheckSyncValidity {
	/**
	 * 检查同步合法性
	 * @return
	 */
	boolean check();
}
