package com.stronglink.esm27.datasync.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.stronglink.esm27.datasync.entity.SyncDataEntity;
import com.stronglink.esm27.datasync.entity.SyncTableEntity;
import com.stronglink.esm27.datasync.entity.SyncTableStructureEntity;

public interface IDataSyncService {
	/**
	 * 同步表格数据
	 * @param srcConnectionParams 	源数据库的连接参数
	 * @param srcTable				源表格名
	 * @param destConnectionParams	目标数据库的连接参数
	 * @param destTable				目标表格名
	 * @param feildSet				表中的字段名数组
	 * @param syncTableParams		同步表所用的参数
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	void syncTable(SyncDataEntity syncData, String srcTable,String destTable,Set<String> feildSet, SyncTableEntity syncTableParams) throws SQLException, ClassNotFoundException;
	
	/**
	 * 获取指定源和目标数据库中相同名的表结构
	 * @param srcConnectionParams	源数据库的连接参数
	 * @param destConnectionParams	目标数据库的连接参数
	 * @param syncColumnName		用于判断是否同步的字段名（标记）
	 * @return
	 * @throws SQLException 
	 */
	SyncTableStructureEntity getSyncTableStructure(String srcConnectionParams, String destConnectionParams,SyncTableEntity[] syncColumnName) throws SQLException;
	
	/**
	 * 清除表中所有数据
	 * @param srcConnectionParams	源数据库的连接参数
	 * @param table	表名
	 * @return		返回删除的数据条数
	 * @throws SQLException 
	 */
	int clearTableContent(String srcConnectionParams, String table) throws SQLException;
	
	/**
	 * 获取表格中的数据内容
	 * @param srcConnectionParams
	 * @param table
	 * @return
	 * @throws SQLException 
	 */
	List<Map<String, Object>> getTableAllContent(String srcConnectionParams, String table) throws SQLException;
	/**
	 * 插入数据到指定表
	 * @param destConnectionParams
	 * @param table
	 * @param srcDatas
	 * @param listener
	 * @return
	 * @throws SQLException 
	 */
	int insert(String destConnectionParams, String table,List<Map<String, Object>> srcDatas,ISQLListener listener) throws SQLException;
	/**
	 * 动态执行更新指定sql
	 * @param srcConnectionParams 需要执行的数据库源参数
	 * @param sql 需要执行的sql
	 * @return 返回执行的条数
	 * @throws SQLException 
	 */
	int updateSql(String srcConnectionParams, String sql) throws SQLException;
}
