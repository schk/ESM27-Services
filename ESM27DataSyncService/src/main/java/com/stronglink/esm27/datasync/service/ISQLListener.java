package com.stronglink.esm27.datasync.service;

import java.sql.ResultSet;

public interface ISQLListener {
	void onConnectioned();
	void onExecuteQuery(String tableName,String sql, ResultSet resultSet);
	void onExecuteUpdate(String tableName,String sql, int resultCount);
	void onClose();
	void onError(Throwable throwable);
}
