package com.stronglink.esm27.datasync.service;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class SyncConfigEntity {
	@Id
	private String syncId;
	
	private Date syncTime;

	public String getSyncId() {
		return syncId;
	}

	public void setSyncId(String syncId) {
		this.syncId = syncId;
	}

	public Date getSyncTime() {
		return syncTime;
	}

	public void setSyncTime(Date syncTime) {
		this.syncTime = syncTime;
	}
	
	
}
