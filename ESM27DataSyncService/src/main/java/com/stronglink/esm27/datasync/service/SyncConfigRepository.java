package com.stronglink.esm27.datasync.service;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SyncConfigRepository extends JpaRepository<SyncConfigEntity,String> {

}
