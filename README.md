# ESM27-Services

#### 项目介绍
ESM27后台服务，用于为前台WEB提供数据支持。

#### 软件架构
气体采集服务
数据同步服务

#### 安装教程

1. ActiveMQ
2. JDK 1.8

#### 使用说明

启动直接运行 java -jar 服务名称.jar
