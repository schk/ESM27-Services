package com.sczc.esm27.collection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Esm27CollectionServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(Esm27CollectionServiceApplication.class, args);
    }

}
