package com.sczc.esm27.collection.core.message;
/**
 * 消息发送器
 * @author yuzhantao
 *
 */
public interface IMessageSender<T> {
	void sendMessage(T datas);
}
