package com.sczc.esm27.collection.entity;

public class DataMessage {
    private String uuid;
    private String type;
    private Object payload;
    private long timestamp;

    public DataMessage(String uuid, String type, Object payload, long timestamp) {
        this.uuid = uuid;
        this.type = type;
        this.payload = payload;
        this.timestamp = timestamp;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
