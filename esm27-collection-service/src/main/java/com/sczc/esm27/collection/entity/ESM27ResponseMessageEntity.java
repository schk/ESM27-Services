package com.sczc.esm27.collection.entity;

public class ESM27ResponseMessageEntity {
    private byte commandCode;
    private byte[] datas;

    public ESM27ResponseMessageEntity(byte commandCode, byte[] datas) {
        this.commandCode = commandCode;
        this.datas = datas;
    }

    public byte getCommandCode() {
        return commandCode;
    }

    public void setCommandCode(byte commandCode) {
        this.commandCode = commandCode;
    }

    public byte[] getDatas() {
        return datas;
    }

    public void setDatas(byte[] datas) {
        this.datas = datas;
    }
}
