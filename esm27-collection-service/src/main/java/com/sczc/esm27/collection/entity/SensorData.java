package com.sczc.esm27.collection.entity;

/**
 * 传感器数据
 */
public class SensorData {
    private String name;        // 传感器名称代码
    private String displayName;  // 传感器显示名称（如 一氧化碳、氢气 等）
    private String unit;        // 传感器单位
    private String status;      // 传感器状态
    private Float value;        // 传感器值
    private String nfcData;     // NFC 数据（仅在处理 NFC 数据时使用）

    /**
     * 传感器数据
     * @param name
     * @param displayName
     * @param unit
     * @param status
     * @param value
     */
    public SensorData(String name, String displayName, String unit, String status, Float value,String nfcData) {
        this.name = name;
        this.displayName = displayName;
        this.unit = unit;
        this.status = status;
        this.value = value;
        this.nfcData = nfcData;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getNfcData() {
        return nfcData;
    }

    public void setNfcData(String nfcData) {
        this.nfcData = nfcData;
    }
}
